<?php

namespace Admin;

use Zend\Router\Http\Segment;

return [
    'admin-dashboard' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/admin/dashboard[/:action][/:id]',
            'defaults' => [
                'controller' => Controller\DashboardController::class,
                'action' => 'index'
            ]
        ]
    ],
    'admin-actions' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/admin/actions[/:action][/:id][/:param]',
            'defaults' => [
                'controller' => Controller\ActionsController::class,
            ]
        ]
    ],
    'admin-interactive' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/admin/interactive/:action',
            'defaults' => [
                'controller' => Controller\InteractiveController::class,
                'action' => 'index'
            ]
        ]
    ],
    'admin-users' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/admin/users[/:action][/:id]',
            'defaults' => [
                'controller' => Controller\ActionsController::class,

            ]
        ]
    ]
];