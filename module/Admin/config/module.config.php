<?php
namespace Admin;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => include 'routes.config.php',
    ],
    'controllers' => [
        'factories' => [
            Controller\DashboardController::class => InvokableFactory::class,
            Controller\InteractiveController::class => InvokableFactory::class,
            Controller\ActionsController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'admin/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'header/scripts' => __DIR__ . '/../../Application/view/layout/header/scripts.phtml',
            'header/styles' => __DIR__ . '/../../Application/view/layout/header/styles.phtml',
            'header/meta' => __DIR__ . '/../../Application/view/layout/header/meta.phtml',
        ],
        'template_path_stack' => [
            'admin' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'mail' => [
        'name'              => 'gmail.com',
        'host'              => 'smtp.gmail.com',
        'port'              => 587, // Notice port change for TLS is 587
        'connection_class'  => 'plain',
        'connection_config' => array(
            'username' => 'vitalii.krushelnytskyi@gmail.com',
            'password' => 'phgtrsbetvdoqcxr',
            'ssl'      => 'tls',
        ),
    ],
    'civic_api' => [
        'key' => 'AIzaSyCycVigonqoJdOAPA5S9Qm5ynkl0ejSrVA'
    ],
    'states' => [
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming',
    ]
];
