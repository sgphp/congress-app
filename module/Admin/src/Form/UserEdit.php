<?php

namespace Admin\Form;

use Zend\Form\Form;

/**
 * Class UserEdit
 */
class UserEdit extends Form
{
    /**
     * UserEdit constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct(null, $options);

        $this->add(
            [
                'type' => \Application\Form\Element\Email::class,
                'name' => 'email',
            ]
        );

        $this->add(
            [
                'type' => \Application\Form\Element\Name::class,
                'name' => 'name',
            ]
        );
    }
    
}