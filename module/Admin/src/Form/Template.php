<?php

namespace Admin\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

/**
 * Class Template
 * @package Admin\Form
 */
class Template extends Form
{

    /**
     * Template constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        parent::__construct(null, []);

        $this->setData($data);

        $this->add(
            [
                'type' => Text::class,
                'name' => 'content',
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'min' => 4,
                            'max' => 36600
                        ]
                    ]
                ]
            ]
        );

        $this->add(
            [
                'type' => Text::class,
                'name' => 'name'
            ]
        );

        $this->add(
            [
                'type' => Text::class,
                'name' => 'category'
            ]
        );

        $this->addInputFilter();
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add(
            [
                'name' => 'content',
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'min' => 4,
                            'max' => 36600
                        ]
                    ]
                ]
            ]
        );

        $inputFilter->add(
            [
                'name' => 'name',
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'min' => 4,
                            'max' => 500
                        ]
                    ]
                ]
            ]
        );
    }
}