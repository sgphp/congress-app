<?php

namespace Admin\Controller;

use Admin\Form\Category;
use Admin\Form\Template;
use Application\Model\Division;
use Application\Model\Letter;
use Application\Model\Office;
use Application\Model\Official;
use Application\Model\Option;
use Application\Model\Question;
use Application\Model\Template as TemplateModel;
use Application\Model\Repository\TagRepository;
use Application\Model\Tag;
use Application\Model\TemplatePipeline;
use Application\Model\TemplateTag;
use Application\Model\User;
use Application\Model\Variant;
use Doctrine\Common\Collections\Criteria;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class ActionsController
 * @package Admin\Controller
 */
class ActionsController extends AbstractController
{

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function storeTemplateAction()
    {
        $form = new Template($this->getRequest()->getPost()->toArray());

        $result = new JsonModel();

        if (false === $form->isValid()) {
            return $result->setVariable('errors', $form->getMessages());
        }

        $data = $form->getData();

        if (null === ($id = $this->getRequest()->getPost('template'))) {
            $template = new TemplateModel();
            $template->setCreated(new \DateTime());
        } else {
            $template = $this->getEntityManager()->getRepository(TemplateModel::class)->find($id);
        }

        $template->setName($data['name']);
        $template->setTemplate($data['content']);
        $template->setEnabled(true);



        if (true === isset($data['category'])) {

            /** @var \Application\Model\Category $category */
            $category = $this->getEntityManager()
                ->getRepository(\Application\Model\Category::class)
                ->find($data['category']);

            if (null !== $category) {
                $template->setCategory($category);
            }

        }

        $this->getEntityManager()->persist($template);
        $this->getEntityManager()->flush($template);

        $pipelinesRepo = $this->getEntityManager()->getRepository(TemplatePipeline::class);

        $pipelines = $pipelinesRepo->findBy(
            [
                'template' => $template
            ]
        );

        /** @var int[] $pipeline */
        $pipelines = array_map(
            function ($pipeline) {
                /** @var TemplatePipeline $pipeline */
                return $pipeline->getId();
            },
            $pipelines
        );

        if (null !== ($divisionPipelines = $this->getRequest()->getPost('divisions')) && true === is_array($divisionPipelines)) {
            foreach ($divisionPipelines as $divisionId) {
                /** @var Division $division */
                $division = $this->getEntityManager()->getRepository(Division::class)->find($divisionId);

                /** @var TemplatePipeline $pipeline */
                $pipeline = $pipelinesRepo->findOneBy(
                    [
                        'division' => $division,
                        'template' => $template
                    ]
                );

                if (null !== $pipeline) {

                    if (true === in_array($pipeline->getId(), $pipelines)) {
                        unset($pipelines[array_search($pipeline->getId(), $pipelines)]);
                    }

                    continue;
                }

                $pipeline = new TemplatePipeline();

                $pipeline->setDivision($division);
                $pipeline->setTemplate($template);

                $this->getEntityManager()->persist($pipeline);
            }
        }

        if (null !== ($officePipelines = $this->getRequest()->getPost('offices')) && true === is_array($officePipelines)) {
            foreach ($officePipelines as $officeId) {

                /** @var Office $office */
                $office = $this->getEntityManager()->getRepository(Office::class)->find($officeId);

                /** @var TemplatePipeline $pipeline */
                $pipeline = $pipelinesRepo->findOneBy(
                    [
                        'office'   => $office,
                        'template' => $template
                    ]
                );

                if (null !== $pipeline) {

                    if (true === in_array($pipeline->getId(), $pipelines)) {
                        unset($pipelines[array_search($pipeline->getId(), $pipelines)]);
                    }

                    continue;
                }

                $pipeline = new TemplatePipeline();

                $pipeline->setOffice($office);
                $pipeline->setTemplate($template);

                $this->getEntityManager()->persist($pipeline);
            }
        }

        if (null !== ($officialPipelines = $this->getRequest()->getPost('officials')) && true === is_array($officialPipelines)) {
            foreach ($officialPipelines as $officialId) {
                /** @var Official $official */
                $official = $this->getEntityManager()->getRepository(Official::class)->find($officialId);

                /** @var TemplatePipeline $pipeline */
                $pipeline = $pipelinesRepo->findOneBy(
                    [
                        'official' => $official,
                        'template' => $template
                    ]
                );

                if (null !== $pipeline) {

                    if (true === in_array($pipeline->getId(), $pipelines)) {
                        unset($pipelines[array_search($pipeline->getId(), $pipelines)]);
                    }

                    continue;
                }

                $pipeline = new TemplatePipeline();

                $pipeline->setOfficial($official);
                $pipeline->setTemplate($template);

                $this->getEntityManager()->persist($pipeline);
            }
        }

        if (null !== ($divisionPipelines = $this->getRequest()->getPost('general'))) {

            /** @var TemplatePipeline $pipeline */
            $pipeline = $pipelinesRepo->findOneBy(
                [
                    'official' => null,
                    'division' => null,
                    'office'   => null,
                    'template' => $template
                ]
            );

            if (null !== $pipeline) {

                if (true === in_array($pipeline->getId(), $pipelines)) {
                    unset($pipelines[array_search($pipeline->getId(), $pipelines)]);
                }
            } else {
                $pipeline = new TemplatePipeline();
                $pipeline->setTemplate($template);

                $this->getEntityManager()->persist($pipeline);
            }
        }

        $criteria = new Criteria();
        $criteria->where($criteria->expr()->in('id', $pipelines));

        $pipelinesRepo->matching($criteria)->forAll(
            function ($pipeline) {
                $this->getEntityManager()->remove($pipeline);
            }
        );

        $this->getEntityManager()->flush();

        if (false === empty($tags = $this->getRequest()->getPost('tags'))) {

            $templateTags = array_map(
                function ($templateTag) {
                    /** @var TemplateTag $templateTag */
                    return $templateTag->getId();
                },
                $this->getEntityManager()
                    ->getRepository(TemplateTag::class)
                    ->findBy(
                        [
                            'template' => $template
                        ]
                    )
            );

            foreach ($tags as $tag) {
                /** @var TagRepository $tagRepository */
                $tagRepository = $this->getEntityManager()
                    ->getRepository(Tag::class);

                $tagModel = $tagRepository->findOneBy(
                    [
                        'value' => $tag
                    ]
                );

                if (null === $tagModel) {
                    $tagModel = new Tag();
                    $tagModel->setValue($tag);
                    $tagModel->setCount(0);
                    $this->getEntityManager()->persist($tagModel);
                    $this->getEntityManager()->flush($tagModel);
                }

                /** @var TemplateTag $templateTag */
                $templateTag = $this->getEntityManager()
                    ->getRepository(TemplateTag::class)
                    ->findOneBy(
                        [
                            'tag' => $tagModel,
                            'template' => $template
                        ]
                    );

                if (null === $templateTag) {
                    $tagRepository->connectTag($tagModel, $template);
                } else {
                    if (true === in_array($templateTag->getId(), $templateTags)) {
                        unset($templateTags[array_search($templateTag->getId(), $templateTags)]);
                    }
                }
            }

            foreach ($templateTags as $templateTag) {
                $this->getEntityManager()->remove($this->getEntityManager()->getRepository(TemplatePipeline::class)->find($templateTag));
            }

            $this->getEntityManager()->flush();
        }

        $questions = $this->getRequest()->getPost('question', []);
        $variants = $this->getRequest()->getPost('variant', []);

        $questionModels = array_map(
            function ($question) {
                /** @var Question $question */
                return $question->getId();
            },
            $this->getEntityManager()
                ->getRepository(Question::class)
                ->findBy(
                    [
                        'template' => $template
                    ]
                )
        );

        foreach ($questions as $index => $question) {

            $questionModel = $this->getEntityManager()
                ->getRepository(Question::class)
                ->findOneBy(
                    [
                        'message'  => $question,
                        'template' => $template
                    ]
                );

            if (null === $questionModel) {
                $questionModel = new Question();
                $questionModel->setTemplate($template);
                $questionModel->setActive(true);
                $questionModel->setAuthor($this->getUser());
                $questionModel->setMessage($question);
                $questionModel->setBeginning($this->getRequest()->getPost('question_beginning')[$index]);

                if (false === empty($variants[$index])) {
                    $questionModel->setFree(false);
                } else {
                    $questionModel->setFree(true);
                }

                $questionModel->setMultiple(
                    (isset($this->getRequest()->getPost('multiples')[$index])
                        && 'on' === $this->getRequest()->getPost('multiples')[$index]
                    ) ? true : false);

                $this->getEntityManager()->persist($questionModel);
                $this->getEntityManager()->flush($questionModel);

                foreach ($variants[$index] as $variantIndex => $variant) {

                    $variantModel = new Variant();
                    $variantModel->setActive(true);

                    if (false === empty($this->getRequest()->getPost('compiled_value')[$index][$variantIndex])) {
                        $compiledValue = $this->getRequest()->getPost('compiled_value')[$index][$variantIndex];
                    } else {
                        $compiledValue = '';
                    }

                    $variantModel->setCompiledValue($compiledValue);
                    $variantModel->setValue($variant);
                    $variantModel->setQuestion($questionModel);

                    $this->getEntityManager()->persist($variantModel);
                }
            }

            if (true === in_array($questionModel->getId(), $questionModels)) {
                unset($questionModels[array_search($questionModel->getId(), $questionModels)]);
            }
        }

        foreach ($questionModels as $questionId) {
            $question = $this->getEntityManager()->getRepository(Question::class)->find($questionId);

            $variants = $this->getEntityManager()
                ->getRepository(Variant::class)
                ->findBy(
                    [
                        'question' => $question
                    ]
                );

            foreach ($variants as $variant) {
                $this->getEntityManager()->remove($variant);
            }

            $this->getEntityManager()->flush();

            $this->getEntityManager()->remove($question);

            $this->getEntityManager()->flush();
        }

        $this->getEntityManager()->flush();

        return $result->setVariable(
            'redirect',
            $this->url()->fromRoute('admin-dashboard', ['action' => 'templates'])
        );
    }

    /**
     * @return JsonModel|ViewModel
     */
    public function storeCategoryAction()
    {
        $form = new Category($this->getRequest()->getPost()->toArray());

        $result = new JsonModel();

        if (false === $form->isValid()) {
            return $result->setVariable('errors', $form->getMessages());
        }

        $data = $form->getData();

        if (null === ($id = $this->getRequest()->getPost('category'))) {
            $category = new \Application\Model\Category();
        } else {
            $category = $this->getEntityManager()->getRepository(\Application\Model\Category::class)->find($id);
        }

        $category->setName($data['name']);
        $category->setDescription($data['description']);
        $category->setSlug(
            strtolower(
                preg_replace('/[\s]/', '-', $category->getName())
            )
        );

        $this->getEntityManager()->persist($category);
        $this->getEntityManager()->flush($category);

        $result->setVariable('redirect', $this->url()->fromRoute('admin-dashboard', ['action' => 'categories']));

        return $result;
    }
    
    /**
     * @return array|ViewModel
     */
    public function showAction()
    {
        $id = $this->params('id');
        if ($id !== null) {
            $user = $this->getEntityManager()->getRepository(User::class)->find($id);
            return new ViewModel(
                [
                    'user' => $user
                ]
            );
        }

        return $this->notFoundAction();
    }

    /**
     * @return array|JsonModel|ViewModel
     */    
    public function editAction()
    {
        $id = $this->params('id');
        if ($id !== null && true == $this->getRequest()->isPost()  && $this->getRequest()->isXmlHttpRequest()) {
            $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['id' => $id]);
            if ($user !== null) {
                $user->setEmail($this->getRequest()->getPost('email'));
                $user->setName($this->getRequest()->getPost('name'));
                $user->setRole($this->getRequest()->getPost('role'));
                $user->getSettings()->setAddressLine($this->getRequest()->getPost('address'));
                $user->getSettings()->setCity($this->getRequest()->getPost('city'));
                $user->getSettings()->setState($this->getRequest()->getPost('state'));
                $user->getSettings()->setZip($this->getRequest()->getPost('zip'));
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();
                return new JsonModel(
                    [
                        'redirect' => $this->url()
                            ->fromRoute(
                                'admin-users',
                                [
                                    'action' => 'show',
                                    'id' => $user->getId()
                                ]
                            ),
                    ]
                );
            }

            return $this->notFoundAction();
        } else {
            if (null !== $id) {
                $user = $this->getEntityManager()
                    ->getRepository(User::class)
                    ->findOneBy(['id' => $id]);

                $config = $this->getEvent()
                    ->getApplication()
                    ->getServiceManager()
                    ->get('config');

                if (null !== $user) {
                    $view = new ViewModel(
                        [
                            'user' => $user,
                            'states' => $config['states'] ?? [],
                            'action'  => $this->url()->fromRoute('admin-users', ['action' => 'edit'])
                        ]
                    );
                    $view->setTemplate('admin/actions/edit');

                    return $view;
                }
            }

            return $this->notFoundAction();
        }
    }

    /**
     * @return \Zend\Http\Response
     */
    public function deleteTemplateAction()
    {
        $id = $this->params('id');

        /** @var TemplateModel $template */
        $template = $this->getEntityManager()->getRepository(TemplateModel::class)->find($id);
        $template->setEnabled(false);

        /** @var TagRepository $tagRepository */
        $tagRepository = $this->getEntityManager()->getRepository(Tag::class);

        $tagRepository->disconnectAllTags($template);

        $this->getEntityManager()->persist($template);
        $this->getEntityManager()->flush();

        return $this->redirect()->toRoute(
            'admin-dashboard',
            [
                'action' => 'templates'
            ],
            [
                'query' => [
                    'enabled' => 'false'
                ]
            ]
        );
    }

    /**
     * @return \Zend\Http\Response
     */
    public function saveOptionsAction()
    {
        $optionRepository = $this->getEntityManager()->getRepository(Option::class);

        foreach ($this->getRequest()->getPost() as $name => $value) {
            $option = $optionRepository->findOneBy(['name' => $name]);

            if (null !== $option) {
                $option->setValue($value);
                $this->getEntityManager()->persist($option);
            }
        }

        $this->getEntityManager()->flush();

        return $this->redirect()->toRoute('admin-dashboard');
    }

    /**
     * @return array|\Zend\Http\Response
     */
    public function markAction()
    {
        $letterId = $this->params('id');

        if (null === $letterId || null === ($letter = $this->getEntityManager()->getRepository(Letter::class)->find($letterId))) {
            return $this->notFoundAction();
        }

        /** @var Letter $letter */

        $letter->setStatus($this->params('param'));

        $this->getEntityManager()->persist($letter);
        $this->getEntityManager()->flush($letter);

        $url = $this->getRequest()->getHeader('Referer')->getUri();
        return $this->redirect()->toUrl($url);
    }

}