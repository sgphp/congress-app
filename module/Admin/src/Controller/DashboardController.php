<?php

namespace Admin\Controller;

use Application\Controller\Concern\PaginatorResource;
use Application\Model\Category;
use Application\Model\Division;
use Application\Model\Letter;
use Application\Model\Office;
use Application\Model\Official;
use Application\Model\Option;
use Application\Model\Question;
use Application\Model\Template;
use Application\Model\TemplatePipeline;
use Application\Model\TemplateTag;
use Application\Model\Transaction;
use Application\Model\Variant;
use Doctrine\Common\Collections\Criteria;
use Application\Model\User;
use Zend\Stdlib\ArrayUtils;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class DashboardController
 * @package Application\Controller\Admin
 */
class DashboardController extends AbstractController
{

    use PaginatorResource;

    /**
     * @return ViewModel
     */
    public function indexAction()
    {

        $optionNames = Option::getConstants('OPTION_');

        $options = [];

        $optionRepository = $this->getEntityManager()->getRepository(Option::class);

        foreach ($optionNames as $optionName) {
            $option = $optionRepository->findOneBy(['name' => $optionName]);

            if (null === $option) {
                $option = new Option();
                $option->setName($optionName);
                $option->setValue('');

                $this->getEntityManager()->persist($option);
                $this->getEntityManager()->flush($option);
            }

            $options[] = $option;
        }

        $userCount = $this->getEntityManager()
            ->getUnitOfWork()
            ->getEntityPersister(User::class)
            ->count(
                [
                    'role' => User::ROLE_USER
                ]
            );

        $letterCount = $this->getEntityManager()
            ->getUnitOfWork()
            ->getEntityPersister(Letter::class)
            ->count(
                [
                    'status' => Letter::STATUS_QUEUE
                ]
            );

        $letterSentCount = $this->getEntityManager()
            ->getUnitOfWork()
            ->getEntityPersister(Letter::class)
            ->count(
                [
                    'status' => Letter::STATUS_CLOSED
                ]
            );

        $transactionCount = $this->getEntityManager()
            ->getUnitOfWork()
            ->getEntityPersister(Transaction::class)
            ->count(
                [
                    'status' => Transaction::STATUS_SUCCESS
                ]
            );


        $lastUsers = $this->getEntityManager()
            ->getRepository(User::class)
            ->findBy(
                [
                    'role' => User::ROLE_USER
                ],
                [
                    'created' => Criteria::DESC
                ],
                4
            );

        $lastLetters = $this->getEntityManager()
            ->getRepository(Letter::class)
            ->findBy(
                [
                ],
                [
                    'created' => Criteria::DESC
                ],
                5
            );

        $notifications = ArrayUtils::merge($lastUsers, $lastLetters);

        usort(
            $notifications,
            function ($first, $second) {
                /** @var User|Letter $first */
                /** @var User|Letter $second */

                return $second->getCreated() <=> $first->getCreated();
            }
        );

        return new ViewModel(
            [
                'options'          => $options,
                'userCount'        => $userCount,
                'letterCount'      => $letterCount,
                'letterSentCount'  => $letterSentCount,
                'transactionCount' => $transactionCount,
                'notifications'    => $notifications
            ]
        );
    }

    /**
     * @return ViewModel
     */
    public function lettersAction()
    {
        return new ViewModel(
            [
                'paginator' => $this->paginator(Letter::class, $this->getRequest()->getQuery('page', 1))
            ]
        );

    }

    /**
     * @return ViewModel
     */
    public function templatesAction()
    {
        $this->criteria->where($this->criteria->expr()->eq('enabled', $this->getRequest()->getQuery('enabled') === 'false' ? false : true));

        return new ViewModel(
            [
                'paginator' => $this->paginator(Template::class, $this->getRequest()->getQuery('page', 1))
            ]
        );
    }

    /**
     * @return ViewModel
     */
    public function addTemplateAction()
    {
        return new ViewModel();
    }

    /**
     * @return ViewModel
     */
    public function categoriesAction()
    {
        return new ViewModel(
            [
                'paginator' => $this->paginator(Category::class, $this->getRequest()->getQuery('page', 1))
            ]
        );
    }

    /**
     * @return ViewModel
     */
    public function addCategoryAction()
    {
        return new ViewModel();
    }

    /**
     * @return ViewModel
     */
    public function editCategoryAction()
    {
        return (new ViewModel(
            [
                'category' => $this->getEntityManager()->getRepository(Category::class)->find($this->params('id'))
            ]
        ))->setTemplate('admin/dashboard/add-category');
    }

    /**
     * @return array|ViewModel
     */
    public function editTemplateAction()
    {
        if (null === ($id = $this->params('id'))
            || null === ($template = $this->getEntityManager()->getRepository(Template::class)->find($id))
        ) {
            return $this->notFoundAction();
        }

        $tagsConnections = $this->getEntityManager()
            ->getRepository(TemplateTag::class)
            ->findBy(
                [
                    'template' => $template
                ]
            );

        $tags = array_map(
            function ($tagConnection) {
                /** @var TemplateTag $tagConnection */
                return $tagConnection->getTag();
            },
            $tagsConnections
        );

        $questions = $this->getEntityManager()
            ->getRepository(Question::class)
            ->findBy(
                [
                    'template' => $template
                ]
            );

        $criteria = new Criteria();
        $criteria->where($criteria->expr()->in('question', $questions));

        $variants = $this->getEntityManager()
            ->getRepository(Variant::class)
            ->matching($criteria);

        return (new ViewModel(
            [
                'template' => $template,
                'pipelines' => $this->getEntityManager()
                    ->getRepository(TemplatePipeline::class)
                    ->findBy(
                        [
                            'template' => $template
                        ]
                    ),
                'questions' => $questions,
                'variants' => $variants,
                'tags' => $tags
            ]
        ))->setTemplate('admin/dashboard/add-template');
    }

    /**
     * @return ViewModel
     */
    public function usersAction()
    {
            $repository = $this->getEntityManager()->getRepository(User::class);
            $users = $repository->findAll();
                return new ViewModel(
                    [
                        'users' => $users
                    ]
                );
    }

    /**
     * @return ViewModel
     */
    public function divisionsAction()
    {
        return new ViewModel([
                'paginator' => $this->paginator(Division::class, $this->getRequest()->getQuery('page', 1))
            ]
        );
    }

    /**
     * @return ViewModel
     */
    public function officesAction()
    {
        if (true == $this->params('id')) {
            $criteria = new Criteria();
            $repository = $this->getEntityManager()->getRepository(Division::class);
            $division = $repository->find($this->params('id'));
            $this->criteria->where($criteria->expr()->eq('division', $division));

            return new ViewModel(
                [
                    'paginator' => $this->paginator(Office::class, $this->getRequest()->getQuery('page', 1))
                ]
            );
        } else {
            return new ViewModel([
                    'paginator' => $this->paginator(Office::class, $this->getRequest()->getQuery('page', 1))
                ]
            );
        }
    }

    /**
     * @return ViewModel
     */
    public function officialsAction()
    {
        if (true == $this->params('id')) {
            $criteria = new Criteria();
            $repository = $this->getEntityManager()->getRepository(Office::class);
            $office = $repository->find($this->params('id'));
            $this->criteria->where($criteria->expr()->eq('office', $office));

            return new ViewModel(
                [
                    'paginator' => $this->paginator(Official::class, $this->getRequest()->getQuery('page', 1))
                ]
            );
        } else {
            return new ViewModel(
                [
                    'paginator' => $this->paginator(Official::class, $this->getRequest()->getQuery('page', 1))
                ]
            );
        }
    }

    /**
     * @return array|JsonModel|ViewModel
     */
    public function editOfficialAction()
    {
        $id = $this->params('id');
        if ($id !== null && true == $this->getRequest()->isPost()  && $this->getRequest()->isXmlHttpRequest()) {
            $official = $this->getEntityManager()->getRepository(Official::class)->findOneBy(['id' => $id]);
            if ($official !== null) {
                $official->setName($this->getRequest()->getPost('name'));
                $address = [
                  [
                      'line1' => $this->getRequest()->getPost('line_1'),
                      'line2' => $this->getRequest()->getPost('line_2'),
                      'city'  => $this->getRequest()->getPost('city'),
                      'state' => $this->getRequest()->getPost('state'),
                      'zip'   => $this->getRequest()->getPost('zip'),
                  ]
                ];
                $official->setAddress($address);
                $official->setParty($this->getRequest()->getPost('party'));
                $official->setPhones(explode(',', $this->getRequest()->getPost('phone')));
                $official->setEmails(explode(',', $this->getRequest()->getPost('email')));
                $official->setUrls(explode(',', $this->getRequest()->getPost('url')));
                $official->setPhotoUrl($this->getRequest()->getPost('url_photo'));

                $this->getEntityManager()->persist($official);
                $this->getEntityManager()->flush();
                return new JsonModel(
                    [
                        'redirect' => $this->url()
                            ->fromRoute(
                                'admin-dashboard',
                                [
                                    'action' => 'officials',
                                ]
                            ),
                    ]
                );
            }

            return $this->notFoundAction();
        } else {
            if (null !== $id) {
                $official = $this->getEntityManager()
                    ->getRepository(Official::class)
                    ->findOneBy(['id' => $id]);
 
                if (null !== $official) {
                    $view = new ViewModel(
                        [
                            'official' => $official,
                            'action'  => $this->url()->fromRoute('admin-users', ['action' => 'edit'])
                        ]
                    );
                    $view->setTemplate('admin/dashboard/edit-official');

                    return $view;
                }
            }

            return $this->notFoundAction();
        }
    }

}