<?php

namespace Admin\Controller;

use Application\Model\User;

/**
 * Class AbstractController
 * @package Admin\Controller
 */
abstract class AbstractController extends \Application\Controller\AbstractController
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (null !== $this->getUser() && $this->getUser()->getRole() === User::ROLE_ADMINISTRATOR) {
            $this->layout('admin/layout');
        }
    }

}