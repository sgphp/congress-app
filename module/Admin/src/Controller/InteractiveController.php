<?php

namespace Admin\Controller;

use Application\Model\Category;
use Application\Model\Tag;
use Application\Service\Letter\Configuration\Legislator;
use Application\Service\Letter\Configuration\User;
use Doctrine\Common\Collections\Criteria;
use Zend\View\Model\JsonModel;

/**
 * Class InteractiveController
 * @package Admin\Controller
 */
class InteractiveController extends AbstractController
{

    /**
     * @return JsonModel
     */
    public function indexAction()
    {
        return new JsonModel(
            [
                'message' => sprintf('Start moving by \'%s\'', $this->url()->fromRoute('admin-interactive', ['action' => 'divisions']))
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function addPipelineAction()
    {
        $config = $this->getEvent()->getApplication()->getServiceManager()->get('Config');

        return new JsonModel(
            [
                'modal' => $this->getRenderer()->render(
                    'layout/concern/admin-modal',
                    [
                        'title' => '',
                        'modal' => $this->getRenderer()->render(
                            'layout/templates/pipeline',
                            [
                                'states' => $config['states']
                            ]
                        )
                    ]
                )
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function variablesAction()
    {
        return new JsonModel(
            [
                'variables' => array_merge(
                    array_values(Legislator::getConstants('LEGISLATOR_')),
                    array_values(User::getConstants('USER_'))
                )
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function searchTagsAction()
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->contains('value', $this->getRequest()->getQuery('term')));
        $criteria->orderBy(
            [
                'count' => Criteria::DESC,
            ]
        );
        $criteria->setMaxResults(20);

        $tags = $this->getEntityManager()->getRepository(Tag::class)->matching($criteria);
        $tags = $tags->map(
            function ($tag) {
                /** @var Tag $tag */
                return $tag->getValue();
            }
        );

        return new JsonModel(
            [
                'tags' => $tags->toArray()
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function searchCategoryAction()
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->contains('name', $this->getRequest()->getQuery('term')));
        $criteria->orWhere($criteria->expr()->contains('slug', $this->getRequest()->getQuery('term')));
        $criteria->orderBy(
            [
                'name' => Criteria::ASC,
            ]
        );
        $criteria->setMaxResults(30);

        $categories = $this->getEntityManager()->getRepository(Category::class)->matching($criteria);
        $categories = $categories->map(
            function ($category) {
                /** @var Category $category */
                return [
                    'id'    => $category->getId(),
                    'text'  => $category->getName(),
                ];
            }
        );

        return new JsonModel(
            [
                'categories' => $categories->toArray()
            ]
        );
    }

}