<?php

namespace Admin;

use Application\Model\User;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;

/**
 * Class Module
 * @package Admin
 */
class Module extends \Application\Module
{

    /**
     * @return string
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [];
    }

    /**
     * @param MvcEvent $event
     * @return Acl
     */
    public function getAcl(MvcEvent $event)
    {
        $acl = parent::getAcl($event);

        foreach (array_keys(include __DIR__ . '/../config/routes.config.php') as $resource) {
            $acl->addResource($resource);
        }

        $acl->addRole(User::ROLE_ADMINISTRATOR, User::ROLE_USER);
        $acl->allow(
            User::ROLE_ADMINISTRATOR,
            [
                'admin-interactive',
                'admin-dashboard',
                'admin-actions',
                'admin-users'
            ]
        );

        return $acl;
    }

}