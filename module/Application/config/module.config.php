<?php
namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => include 'routes.config.php',
    ],
    'controllers' => [
        'factories' => [
            Controller\UserController::class => InvokableFactory::class,
            Controller\IndexController::class => InvokableFactory::class,
            Controller\LetterController::class => InvokableFactory::class,
            Controller\Rest\CivicApiController::class => InvokableFactory::class,
            Controller\Rest\ServiceController::class => InvokableFactory::class,
            Controller\Cli\CivicController::class => InvokableFactory::class,
            Controller\LetterIpnController::class => InvokableFactory::class,
            Controller\FramesController::class => InvokableFactory::class,
        ],
    ],
    'controller_plugins' => [
        'invokables' => [
            'api' => Controller\Plugin\Api::class,
        ]
    ],
    'console' => [
        'router' => [
            'routes' => include 'console-routes.config.php'
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/admin/dashboard' => __DIR__ . '/../view/application/admin/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            'application' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'mail' => [
        'name'              => 'gmail.com',
        'host'              => 'smtp.gmail.com',
        'port'              => 587, // Notice port change for TLS is 587
        'connection_class'  => 'plain',
        'connection_config' => array(
            'username' => 'vitalii.krushelnytskyi@gmail.com',
            'password' => 'phgtrsbetvdoqcxr',
            'ssl'      => 'tls',
        ),
    ],
    'locales' => [
        'en_US' => 'English',
        'fr_FR' => 'Francais',
        'de_DE' => 'Deutsch',
    ],
    'civic_api' => [
        'key' => 'AIzaSyCycVigonqoJdOAPA5S9Qm5ynkl0ejSrVA'
    ],
    'states' => [
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming',
    ],
    'paypal' => [
        'client_id'  => 'AYG4TBRN6bzrtMqTZzfL6S37X6EkSMKZpML7XDMwXnXaU0FxGW5jBYljz0pnx8cyjuhA-1sPE_JPcXR8',
        'secret_key' => 'EKetU71oSku7cH_0WoF3dQrpgX_9V-6YkUKCyC8LRYraIpBwYnVPxmBeBKO_IcAPSjXpZXS2Gt_e-FIw',
    ],
    'officials_info' => [
        'csv' => 'https://raw.githubusercontent.com/jlgoldman/membersofcongress/master/members.csv'
    ]
];
