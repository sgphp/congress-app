<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Regex;
use Zend\Router\Http\Segment;

return [
    'home' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action' => 'index',
            ],
        ],
    ],
    'index' => [
        'type' => Segment::class,
        'options' => [
            'route' => '[/:action]',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index'
            ],
        ],
    ],
    'user' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/user[/:action][/:key]',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'login'
            ],
        ],
        'constraints' => [
            'action' => '(?!show)'
        ]
    ],
    'show-user' => [
        'type'    => Regex::class,
        'options' => [
            'regex' => '/user/(?<id>[0-9]+)',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action'     => 'show'
            ],
            'spec' => '/user/%id%'
        ]
    ],
     'recovery-password' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/recover-password/:hash',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'recover-password'
            ],
        ],
    ],
    'recovery-password-cancel' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/recover-password-cancel/:hash',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'recover-password-cancel'
            ],
        ],
    ],
    'letter' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/letter[/][:action]',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action'     => 'index'
            ]
        ]
    ],
    'frames' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/frames[/][:action]',
            'defaults' => [
                'controller' => Controller\FramesController::class,
                'action'     => 'index'
            ]
        ]
    ],
    'category-letter' => [
        'type'    => Segment::class,
        'options' => [
            'route'    => '/letters/category/:category',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action'     => 'category-letters'
            ]
        ]

    ],
    'tag-letter' => [
        'type'    => Segment::class,
        'options' => [
            'route'    => '/letters/tag/:tag',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action'     => 'tag-letters'
            ]
        ]
    ],
    'compose-letter' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/letter/compose/:official[/:name]',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action'     => 'compose'
            ]
        ]
    ],
    'compose-letter-template' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/letter/compose-template/:template[/:official]',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action'     => 'compose-template'
            ]
        ]
    ],
    'choose-letter-template-legislator' => [
        'type' => Segment::class,
        'options' => [
            'route'    => '/letter/choose/:template',
            'defaults' => [
                'controller' => Controller\LetterController::class,
                'action'     => 'choose'
            ]
        ]
    ],
    'civic_api' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/rest/civic/:action',
            'defaults' => [
                'controller' => Controller\Rest\CivicApiController::class,
            ]
        ]
    ],
    'service' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/rest/service/:action',
            'defaults' => [
                'controller' => Controller\Rest\ServiceController::class,
            ]
        ]
    ],
    'letter-ipn' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/letter-ipn/:action/:transaction',
            'defaults' => [
                'controller' => Controller\LetterIpnController::class
            ]
        ]
    ]
];