<?php

namespace Application;

use Application\Service\Options;
use Application\Service\Payment\Letter;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Renderer\PhpRenderer;

return [
    'factories'=> [
        'auth' => function(ServiceManager $serviceManager) {
            $storage = new Auth\Storage('auth');
            $storage->setEntityManager($serviceManager->get('Doctrine\ORM\EntityManager'));

            $authService = new \Zend\Authentication\AuthenticationService();
            $authService->setStorage($storage);
            return $authService;
        },
        'mail' => function (ServiceManager $serviceManager) {
            $config = $serviceManager->get('config');
            $smtpOptions = $config['mail'] ?? [];
            $renderer = $serviceManager->get('Zend\View\Renderer\PhpRenderer');

            return new Mail\Sender($smtpOptions, $renderer);
        },
        'letterPayment' => function (ServiceManager $serviceManager) {
            $config = $serviceManager->get('config');
            return new Letter($config['paypal']['client_id'] ?? null, $config['paypal']['secret_key'] ?? null);
        },
        'options' => function () {
            return new Options();
        }
    ],
];