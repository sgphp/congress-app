<?php

namespace Application;

use Application\View\Helper\Options;
use Application\View\Helper\Query;
use Zend\Mvc\Application;
use Zend\ServiceManager\ServiceManager;

return [
    'factories' => [
        'route' => function (ServiceManager $serviceManager) {
            $route = new View\Helper\Route($serviceManager->get('Application')->getMvcEvent()->getRouteMatch());
            return $route;
        },
        'query' => function (ServiceManager $serviceManager) {
            /** @var Application $application */
            $application = $serviceManager->get('Application');
            return new Query($application->getMvcEvent()->getRequest()->getQuery()->toArray());
        },
        'options' => function(ServiceManager $serviceManager) {
            return new Options($serviceManager);
        }
    ]
];