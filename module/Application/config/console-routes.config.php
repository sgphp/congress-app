<?php

namespace Application;

return [
    'civic-grab' => [
        'options' => [
            'route'    => 'civic grab',
            'defaults' => [
                'controller' => Controller\Cli\CivicController::class,
                'action'     => 'grab'
            ]
        ]
    ],
    'civic-additional-grab' => [
        'options' => [
            'route'    => 'civic grab-additional',
            'defaults' => [
                'controller' => Controller\Cli\CivicController::class,
                'action'     => 'grab-additional'
            ]
        ]
    ]
];