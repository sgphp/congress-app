<?php

namespace Application\Model\Repository;

use Application\Model\Tag;
use Application\Model\Template;
use Application\Model\TemplateTag;
use Doctrine\ORM\EntityRepository;

/**
 * Class TagRepository
 * @package Application\Model\Repository
 */
class TagRepository extends EntityRepository
{

    /**
     * @param Tag $tag
     * @param Template $template
     * @return TemplateTag
     */
    public function connectTag(Tag $tag, Template $template)
    {
        $tag->setCount($tag->getCount() + 1);

        $this->getEntityManager()->persist($tag);
        $this->getEntityManager()->flush($tag);

        $templateTag = new TemplateTag();
        $templateTag->setTemplate($template);
        $templateTag->setTag($tag);

        $this->getEntityManager()->persist($templateTag);
        $this->getEntityManager()->flush($templateTag);

        return $templateTag;
    }

    /**
     * @param Tag $tag
     * @param Template $template
     */
    public function disconnectTag(Tag $tag, Template $template)
    {
        $tag->setCount($tag->getCount() - 1);

        $this->getEntityManager()->persist($tag);
        $this->getEntityManager()->flush($tag);

        $connection = $this->getEntityManager()
            ->getRepository(TemplateTag::class)
            ->findOneBy(
                [
                    'tag'      => $tag,
                    'template' => $template
                ]
            );

        if (null !== $connection) {
            $this->getEntityManager()->remove($connection);
            $this->getEntityManager()->flush($connection);
        }
    }

    /**
     * @param $template
     */
    public function disconnectAllTags($template)
    {
        /** @var TemplateTag[] $connections */
        $connections = $this->getEntityManager()
            ->getRepository(TemplateTag::class)
            ->findBy(
                [
                    'template' => $template
                ]
            );

        foreach ($connections as $connection) {
            $connection->getTag()->setCount($connection->getTag()->getCount() - 1);
            $this->getEntityManager()->persist($connection->getTag());
        }

        $this->getEntityManager()->flush();
    }

}