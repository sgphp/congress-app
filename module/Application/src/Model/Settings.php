<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Settings
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="settings")
*/
class Settings
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(length=1023, type="string", name="address_line")
     */
    private $addressLine;

    /**
     * @var string
     * @ORM\Column(length=1023, type="string")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(length=255, type="string")
     */
    private $zip;

    /**
     * @var string
     * @ORM\Column(length=1023, type="string")
     */
    private $state;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddressLine() : string
    {
        return $this->addressLine;
    }

    /**
     * @return string
     */
    public function getCity() : string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getZip() : string
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getState() : string
    {
        return $this->state;
    }

    /**
     * @param string $addressLine
     * @return $this
     */
    public function setAddressLine(string $addressLine)
    {
        $this->addressLine = $addressLine;

		return $this;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity(string $city)
    {
        $this->city = $city;

		return $this;
    }

    /**
     * @param string $zip
     * @return $this
     */
    public function setZip(string $zip)
    {
        $this->zip = $zip;

		return $this;
    }

    /**
     * @param string $state
     * @return $this
     */
    public function setState(string $state)
    {
        $this->state = $state;

		return $this;
    }

}