<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Template
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="template_pipelines")
 */
class TemplatePipeline
{

    /**
     * Types of pipelines
     */
    const TYPE_NONE     = 'none';
    const TYPE_DIVISION = 'division';
    const TYPE_OFFICE   = 'office';
    const TYPE_OFFICIAL = 'official';

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Division
     * @ORM\ManyToOne(targetEntity="Division")
     * @ORM\JoinColumn(name="division_id", referencedColumnName="id")
     */
    private $division;

    /**
     * @var Office
     * @ORM\ManyToOne(targetEntity="Office")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id")
     */
    private $office;

    /**
     * @var Official
     * @ORM\ManyToOne(targetEntity="Official")
     * @ORM\JoinColumn(name="official_id", referencedColumnName="id")
     */
    private $official;

    /**
     * @var Template
     * @ORM\ManyToOne(targetEntity="Template")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Division
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @return Office
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @return Official
     */
    public function getOfficial()
    {
        return $this->official;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @param Division $division
     */
    public function setDivision(Division $division)
    {
        $this->division = $division;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office)
    {
        $this->office = $office;
    }

    /**
     * @param Official $official
     */
    public function setOfficial(Official $official)
    {
        $this->official = $official;
    }

    /**
     * @param Template $template
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }

}