<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Division
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="offices")
 */
class Office
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Division
     * @ORM\ManyToOne(targetEntity="Division", cascade={"persist"})
     * @ORM\JoinColumn(name="division_id", referencedColumnName="id")
     */
    private $division;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $levels;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $roles;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Division
     */
    public function getDivision(): Division
    {
        return $this->division;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getLevels(): array
    {
        return $this->levels;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param Division $division
     */
    public function setDivision(Division $division)
    {
        $this->division = $division;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param array $levels
     */
    public function setLevels(array $levels)
    {
        $this->levels = $levels;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

}