<?php

namespace Application\Model;

use Application\Model\AbstractModel\Concern\ConstantReflector;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Option
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="options")
 */
class Option
{

    /**
     * Option names
     */
    const OPTION_RESPONSIBLE_LESS_100       = 'responsible_less_100';
    const OPTION_RESPONSIBLE_100_500        = 'responsible_100_500';
    const OPTION_RESPONSIBLE_500_1000       = 'responsible_500_1000';
    const OPTION_RESPONSIBLE_1000_5000      = 'responsible_1000_5000';
    const OPTION_RESPONSIBLE_5000_10000     = 'responsible_5000_10000';
    const OPTION_MAIL_PRICE_LESS_100        = 'mail_price_less_100';
    const OPTION_MAIL_PRICE_100_500         = 'mail_price_100_500';
    const OPTION_MAIL_PRICE_500_1000        = 'mail_price_500_1000';
    const OPTION_MAIL_PRICE_1000_5000       = 'mail_price_1000_5000';
    const OPTION_MAIL_PRICE_5000_10000      = 'mail_price_5000_10000';
    const OPTION_MAIL_PRICE_ADDITIONAL_PAGE = 'mail_price_additional_page';
    const OPTION_FAX_PRICE_LESS_100         = 'fax_price_less_100';
    const OPTION_FAX_PRICE_100_500          = 'fax_price_100_500';
    const OPTION_FAX_PRICE_500_1000         = 'fax_price_500_1000';
    const OPTION_FAX_PRICE_1000_5000        = 'fax_price_1000_5000';
    const OPTION_FAX_PRICE_5000_10000       = 'fax_price_5000_10000';
    const OPTION_FAX_PRICE_ADDITIONAL_PAGE  = 'fax_price_additional_page';

    use ConstantReflector;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Return an option value
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Set an option name
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Set an option value
     *
     * @param string $value Option value
     *
     * @return void
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

}