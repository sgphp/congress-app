<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SignatureImage
 * @package Model
 * @ORM\Entity
 * @ORM\Table(name="signatures_images")
 */
class SignatureImage
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $value;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

}