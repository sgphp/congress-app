<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Variant
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="variants")
 */
class Variant
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Question
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(type="text", name="compiled_value")
     */
    private $compiledValue;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $active;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCompiledValue(): string
    {
        return $this->compiledValue;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param Question $question
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @param string $compiledValue
     */
    public function setCompiledValue(string $compiledValue)
    {
        $this->compiledValue = $compiledValue;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

}