<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LetterCount
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="letters_count")
 */
class LetterCount
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $week;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $week
     */
    public function setWeek(int $week)
    {
        $this->week = $week;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year)
    {
        $this->year = $year;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getIntervalName()
    {
        switch (true) {
            case $this->count < 100:
                return 'less_100';
            case $this->count < 500:
                return '100_500';
            case $this->count < 1000:
                return '500_1000';
            case $this->count < 5000:
                return '1000_5000';
            case $this->count < 10000:
                return '5000_10000';
            default:
                return 'less_100';
        }
    }

}