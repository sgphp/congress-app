<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Answer
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="transactions")
 */
class Transaction
{

    /**
     * Transaction payment statuses
     */
    const STATUS_WAIT    = 'wait';
    const STATUS_CANCEL  = 'cancel';
    const STATUS_SUCCESS = 'success';

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default": Transaction::STATUS_WAIT})
     */
    private $status;


    /**
     * @var string
     * @ORM\Column(type="string", name="payment_id")
     */
    private $paymentId;

    /**
     * @var string
     * @ORM\Column(type="string", name="approve_url")
     */
    private $approveUrl;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @return string
     */
    public function getApproveUrl(): string
    {
        return $this->approveUrl;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @param string $paymentId
     */
    public function setPaymentId(string $paymentId)
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @param string $approveUrl
     */
    public function setApproveUrl(string $approveUrl)
    {
        $this->approveUrl = $approveUrl;
    }

}