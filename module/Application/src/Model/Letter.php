<?php

namespace Application\Model;

use Application\Model\AbstractModel\Concern\ConstantReflector;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Template
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="letters")
 */
class Letter
{

    /**
     * Statuses
     */
    const STATUS_CLOSED    = 'closed';
    const STATUS_QUEUE     = 'queue';
    const STATUS_OPEN      = 'open';
    const STATUS_PROGRESS  = 'in progress';
    const STATUS_CANCELED  = 'error';

    /**
     * Letter types
     */
    const TYPE_FAX     = 'Fax';
    const TYPE_US_MAIL = 'US Mail';

    use ConstantReflector;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default": Letter::STATUS_OPEN})
     */
    private $status;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default": "Fax"}, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(type="string", name="responsible_email", nullable=true)
     */
    private $responsibleEmail;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $pdf;

    /**
     * @var Transaction
     * @ORM\ManyToOne(targetEntity="Transaction")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    private $transaction;

    /**
     * @var Template
     * @ORM\ManyToOne(targetEntity="Template")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var Official
     * @ORM\ManyToOne(targetEntity="Official")
     * @ORM\JoinColumn(name="official_id", referencedColumnName="id")
     */
    private $official;

    /**
     * @var float
     * @ORM\Column(type="float", precision=5, scale=3)
     */
    private $price;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getResponsibleEmail(): string
    {
        return $this->responsibleEmail;
    }

    /**
     * @return string
     */
    public function getPdf(): string
    {
        return $this->pdf;
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return Official
     */
    public function getOfficial()
    {
        return $this->official;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @param string $responsibleEmail
     */
    public function setResponsibleEmail(string $responsibleEmail)
    {
        $this->responsibleEmail = $responsibleEmail;
    }

    /**
     * @param string $pdf
     */
    public function setPdf(string $pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @param Transaction $transaction
     */
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @param Template $template
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }

    /**
     * @param Official $official
     */
    public function setOfficial(Official $official)
    {
        $this->official = $official;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

}