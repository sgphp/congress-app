<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package Application\Model
 * @ORM\Entity(repositoryClass="Application\Model\Repository\TagRepository")
 * @ORM\Table(name="tags")
 */
class Tag
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $count;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = (int)$count;
    }

}