<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Division
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="officials")
 */
class Official
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Office
     * @ORM\ManyToOne(targetEntity="Office", cascade={"persist"})
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id")
     */
    private $office;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $party;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $phones;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $emails;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $urls;

    /**
     * @var string
     * @ORM\Column(type="string", name="photo_url", nullable=true)
     */
    private $photoUrl;

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $channels;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getParty()
    {
        return $this->party;
    }

    /**
     * @return array
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    /**
     * @return array
     */
    public function getEmails(): array
    {
        return $this->emails;
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    /**
     * @return array
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office)
    {
        $this->office = $office;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param array $address
     */
    public function setAddress(array $address)
    {
        $this->address = $address;
    }

    /**
     * @param string $party
     */
    public function setParty($party)
    {
        $this->party = $party;
    }

    /**
     * @param array $phones
     */
    public function setPhones(array $phones)
    {
        $this->phones = $phones;
    }

    /**
     * @param array $emails
     */
    public function setEmails(array $emails)
    {
        $this->emails = $emails;
    }

    /**
     * @param array $urls
     */
    public function setUrls(array $urls)
    {
        $this->urls = $urls;
    }

    /**
     * @param string $photoUrl
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
    }

    /**
     * @param array $channels
     */
    public function setChannels(array $channels)
    {
        $this->channels = $channels;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

}