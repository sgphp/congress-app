<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Division
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="divisions")
 */
class Division
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="ocd_id")
     */
    private $ocdId;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOcdId(): string
    {
        return $this->ocdId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $ocdId
     */
    public function setOcdId(string $ocdId)
    {
        $this->ocdId = $ocdId;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

}