<?php

namespace Application\Model;

use Application\Model\AbstractModel\Concern\ConstantReflector;
use Doctrine\ORM\Mapping as ORM;

use Application\Model\SignatureImage;

/**
 * Class User
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{

    /**
     * User roles
     */
    const ROLE_USER          = 'user';
    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_SUSPENDED     = 'suspended';

    use ConstantReflector;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Settings
     * @ORM\ManyToOne(targetEntity="Settings")
     * @ORM\JoinColumn(name="settings_id", referencedColumnName="id")
     */
    private $settings;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $role;

    /**
     * @var Signature
     * @ORM\ManyToOne(targetEntity="Signature")
     * @ORM\JoinColumn(name="signature_id", referencedColumnName="id")
     */
    private $signature;

    /**
     * @var SignatureImage
     * @ORM\ManyToOne(targetEntity="SignatureImage")
     * @ORM\JoinColumn(name="signature_image_id", referencedColumnName="id")
     */
    private $signatureImage;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Settings
     */
    public function getSettings() : Settings
    {
        return $this->settings;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return Signature
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @return SignatureImage
     */
    public function getSignatureImage(): SignatureImage
    {
        return $this->signatureImage;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Settings $settings
     * @return $this
     */
    public function setSettings(Settings $settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @param array|string  $roles  Array on one value of allowed user roles
     * @return bool
     */
    public function roleCheck($roles): bool
    {
        return in_array($this->getRole(), (array)$roles);
    }

    /**
     * @param Signature $signature
     * @return $this
     */
    public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @param SignatureImage $signatureImage
     */
    public function setSignatureImage(SignatureImage $signatureImage)
    {
        $this->signatureImage = $signatureImage;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @param $password
     * @return string
     */
    public static function hashPassword($password)
    {
        return sha1(sha1($password . md5(strrev($password))));
    }

}