<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Question
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="questions")
 */
class Question
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @var Template
     * @ORM\ManyToOne(targetEntity="Template")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $active;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $free;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $multiple;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $beginning;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->free;
    }

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @return string
     */
    public function getBeginning(): string
    {
        return $this->beginning;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @param Template $template
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @param bool $free
     */
    public function setFree(bool $free)
    {
        $this->free = $free;
    }

    /**
     * @param boolean $multiple
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }

    /**
     * @param string $beginning
     */
    public function setBeginning(string $beginning)
    {
        $this->beginning = $beginning;
    }

}