<?php

namespace Application\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Signature
 * @package Application\Model
 * @ORM\Entity
 * @ORM\Table(name="signature")
 */
class Signature
{
    /**
     * Signature types
     */
    const TEXT_TYPE  = 'text';
    const IMAGE_TYPE = 'image';
    
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column (type="string", length=255)
     */
    private $picture;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @var
     * @ORM\Column(type="string")
     */
    private $font;

    /**
     * @return int
     * @ORM\Column(type="string")
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPicture() : string
    {
        return $this->picture;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getFont() : string
    {
        return $this->font;
    }

    /**
     * @param string $picture
     * @return $this
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type) 
    {
        $this->type = $type;
        
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param string $font
     * @return $this
     */
    public function setFont(string $font)
    {
        $this->font = $font;

        return $this;
    }

}