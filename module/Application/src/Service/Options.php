<?php

namespace Application\Service;
use Application\Model\Option;
use Application\Module;

/**
 * Class Options
 * @package Application\Service
 */
class Options
{

    /**
     * @param $name
     * @return string|null
     */
    public function get($name)
    {
        /** @var Option $option */
        $option = Module::entityManager()
            ->getRepository(Option::class)
            ->findOneBy(
                [
                    'name' => $name
                ]
            );

        if (null !== $option) {
            return $option->getValue();
        }

        return null;
    }

}