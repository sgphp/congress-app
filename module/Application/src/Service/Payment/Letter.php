<?php

namespace Application\Service\Payment;

use Application\Module;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 * Class Letter
 * @package Application\Service\Payment
 */
class Letter
{

    /**
     * @var ApiContext
     */
    private $apiContext;

    /**
     * Letter constructor.
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct(string $clientId, string $clientSecret)
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential($clientId,$clientSecret)
        );
    }

    /**
     * @param \Application\Model\Transaction $transactionModel
     * @param float                          $price
     * @param string                         $baseUrl
     *
     * @return Payment
     */
    public function getPayment($transactionModel, $price, $baseUrl)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $total = 0.0;
        $items = new ItemList();

        /** @var \Application\Model\Letter[] $letters */
        $letters = Module::entityManager()
            ->getRepository(\Application\Model\Letter::class)
            ->findBy(
                [
                    'transaction' => $transactionModel
                ]
            );

        foreach ($letters as $letter) {
            $letterItem = new Item();
            $letterItem->setName('Letter ' . $letter->getId())
                ->setQuantity(1)
                ->setPrice(sprintf('%0.2f', $price))
                ->setCurrency('USD');

            $items->addItem($letterItem);

            $total += $price;
        }

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal(sprintf('%0.2f', $total));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($items)
            ->setDescription('Letter payment')
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($baseUrl . 'letter-ipn/success/' . $transactionModel->getId())
            ->setCancelUrl($baseUrl . 'letter-ipn/cancel/' . $transactionModel->getId());

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        return $payment->create($this->apiContext);
    }

}