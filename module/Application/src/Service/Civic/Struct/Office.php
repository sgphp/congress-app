<?php

namespace Application\Service\Civic\Struct;

/**
 * Class Office
 * @package Application\Service\Civic\Struct
 */
class Office
{

    /**
     * Input array key names
     */
    const FIELD_NAME      = 'name';
    const FIELD_LEVELS    = 'levels';
    const FIELD_ROLES     = 'roles';
    const FIELD_OFFICIALS = 'officialIndices';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $levels;

    /**
     * @var array
     */
    protected $roles;

    /**
     * @var Official[]
     */
    protected $officials = [];

    /**
     * @var Division
     */
    protected $division;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getLevels(): array
    {
        return $this->levels;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return Official[]
     */
    public function getOfficials(): array
    {
        return $this->officials;
    }

    /**
     * @return Division
     */
    public function getDivision(): Division
    {
        return $this->division;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

		return $this;
    }

    /**
     * @param array $levels
     * @return $this
     */
    public function setLevels($levels)
    {
        $this->levels = $levels;

		return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

		return $this;
    }

    /**
     * @param Official[] $officials
     * @return $this
     */
    public function setOfficials(array $officials)
    {
        $this->officials = $officials;

		return $this;
    }

    /**
     * @param Official $official
     * @return $this
     */
    public function appendOfficial(Official $official)
    {
        $this->officials[] = $official;

		return $this;
    }

    /**
     * @param Division $division
     */
    public function setDivision(Division $division)
    {
        $this->division = $division;
    }

}