<?php

namespace Application\Service\Civic\Struct;

/**
 * Class Division
 * @package Application\Service\Civic\Struct
 */
class Division
{

    /**
     * Input array key names
     */
    const FIELD_NAME    = 'name';
    const FIELD_OFFICES = 'officeIndices';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Office[]
     */
    protected $offices = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Office[]
     */
    public function getOffices(): array
    {
        return $this->offices;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param Office[] $offices
     */
    public function setOffices(array $offices)
    {
        $this->offices = $offices;
    }

    /**
     * @param Office $office
     */
    public function appendOffice(Office $office)
    {
        $this->offices[] = $office;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return urlencode($this->getId());
    }

}