<?php

namespace Application\Service\Civic\Struct;

/**
 * Class Official
 * @package Application\Service\Civic\Struct
 */
class Official
{

    /**
     * Array field key names
     */
    const FIELD_NAME      = 'name';
    const FIELD_ADDRESS   = 'address';
    const FIELD_PARTY     = 'party';
    const FIELD_EMAILS    = 'emails';
    const FIELD_PHONES    = 'phones';
    const FIELD_URLS      = 'urls';
    const FIELD_PHOTO_URL = 'photoUrl';
    const FIELD_CHANNELS  = 'channels';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $address;

    /**
     * @var string
     */
    protected $party;

    /**
     * @var array
     */
    protected $emails;

    /**
     * @var array
     */
    protected $phones;

    /**
     * @var array
     */
    protected $urls;

    /**
     * @var string
     */
    protected $photoUrl;

    /**
     * @var array
     */
    protected $channels;

    /**
     * @var Office
     */
    protected $office;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getAddress(): array
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getParty()
    {
        return $this->party;
    }

    /**
     * @return array
     */
    public function getEmails(): array
    {
        return $this->emails;
    }

    /**
     * @return array
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    /**
     * @return array
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

		return $this;
    }

    /**
     * @param array $address
     * @return $this
     */
    public function setAddress(array $address)
    {
        $this->address = $address;

		return $this;
    }

    /**
     * @param string $party
     * @return $this
     */
    public function setParty($party)
    {
        $this->party = $party;

		return $this;
    }

    /**
     * @param array $emails
     * @return $this
     */
    public function setEmails(array $emails)
    {
        $this->emails = $emails;

		return $this;
    }

    /**
     * @param array $phones
     * @return $this
     */
    public function setPhones(array $phones)
    {
        $this->phones = $phones;

		return $this;
    }

    /**
     * @param array $urls
     * @return $this
     */
    public function setUrls(array $urls)
    {
        $this->urls = $urls;

		return $this;
    }

    /**
     * @param string $photoUrl
     * @return $this
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;

		return $this;
    }

    /**
     * @param array $channels
     * @return $this
     */
    public function setChannels(array $channels)
    {
        $this->channels = $channels;

		return $this;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office)
    {
        $this->office = $office;
    }
    
}