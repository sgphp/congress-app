<?php

namespace Application\Service\Civic;
use Application\Service\Civic\Struct\Division;
use Application\Service\Civic\Struct\Office;
use Application\Service\Civic\Struct\Official;

/**
 * Class Officials
 * @package Application\Service\Civic
 */
class DataManager
{

    /**
     * @var Official[]
     */
    private $officials = [];

    /**
     * @var Office[]
     */
    private $offices = [];

    /**
     * @var Division[]
     */
    private $divisions = [];

    /**
     * Officials constructor.
     * @param $infoArray
     */
    public function __construct($infoArray)
    {
        $this->officials = array_map(
            function ($officialArray) {
                $official = new Official();
                $official->setName($officialArray[Official::FIELD_NAME])
                    ->setAddress($officialArray[Official::FIELD_ADDRESS] ?? [])
                    ->setEmails($officialArray[Official::FIELD_EMAILS] ?? [])
                    ->setPhones($officialArray[Official::FIELD_PHONES] ?? [])
                    ->setUrls($officialArray[Official::FIELD_URLS] ?? [])
                    ->setParty($officialArray[Official::FIELD_PARTY] ?? null)
                    ->setPhotoUrl($officialArray[Official::FIELD_PHOTO_URL] ?? null)
                    ->setChannels($officialArray[Official::FIELD_CHANNELS] ?? []);

                return $official;
            },
            $infoArray['officials'] ?? []
        );

        $this->offices = array_map(
            function ($officeArray) {
                $office = new Office();
                $office->setName($officeArray[Office::FIELD_NAME])
                    ->setLevels($officeArray[Office::FIELD_LEVELS] ?? [])
                    ->setRoles($officeArray[Office::FIELD_ROLES] ?? []);

                foreach ($officeArray[Office::FIELD_OFFICIALS] ?? [] as $id) {
                    if (true === isset($this->officials[$id])) {
                        $office->appendOfficial($this->officials[$id]);
                        $this->officials[$id]->setOffice($office);
                    }
                }

                return $office;
            },
            $infoArray['offices'] ?? []
        );

        foreach ($infoArray['divisions'] ?? [] as $divisionId => $divisionArray) {
            $division = new Division();
            $division->setId($divisionId);
            $division->setName($divisionArray[Division::FIELD_NAME]);

            $divisionOffices = $divisionArray[Division::FIELD_OFFICES] ?? [];

            $division->setOffices(

                array_filter(
                    $this->offices,
                    function ($index) use ($divisionOffices, $division) {
                        if (true === in_array($index, $divisionOffices)) {
                            $this->offices[$index]->setDivision($division);
                            return true;
                        }

                        return false;
                    },
                    ARRAY_FILTER_USE_KEY
                )

            );

            $this->divisions[] = $division;
        }
    }

    /**
     * @return array
     */
    public function getDivisions()
    {
        return $this->divisions;
    }

    /**
     * @return array
     */
    public function getOffices()
    {
        return $this->offices;
    }

    /**
     * @return array
     */
    public function getOfficials()
    {
        return $this->officials;
    }

}