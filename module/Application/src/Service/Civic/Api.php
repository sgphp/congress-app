<?php

namespace Application\Service\Civic;
use GuzzleHttp\Client;
use Zend\Http\Request;

/**
 * Class Api
 * @package Application\Service\Civic
 */
class Api
{

    /**
     * Civic API constants
     */
    const API_URL                    = 'https://www.googleapis.com/civicinfo/v2/';
    const API_CONTENT_URL            = 'https://content.googleapis.com/civicinfo/v2/';
    const METHOD_ELECTIONS           = 'elections';
    const METHOD_VOTER_INFO          = 'voterinfo';
    const METHOD_REPRESENTATIVE_INFO = 'representatives';

    /**
     * @var string
     */
    private $key;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $requestQueue = [];

    /**
     * Api constructor.
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
        $this->client = new Client();
    }

    /**
     * @param $method
     * @param array $params
     * @param string $httpMethod
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request($method, array $params = [], $httpMethod = Request::METHOD_GET)
    {
        $url = $this->url($method, $params);
        return $this->client->request($httpMethod, $url);
    }

    public function addToQueue($url)
    {
        $this->requestQueue[] = $url;
    }

    /**
     * @param $method
     * @param array $params
     * @param string $httpMethod
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function requestContent($method, array $params = [], $httpMethod = Request::METHOD_GET)
    {
        $url = $this->url($method, $params, static::API_CONTENT_URL);
        return $this->client->request($httpMethod, $url);
    }

    /**
     * @param $method
     * @param array $params
     * @param string $base
     * @return string
     */
    public function url($method, array $params = [], $base = self::API_URL)
    {
        return $base . $method . '?' . http_build_query(
            array_merge(
                [
                    'key' => $this->key,
                ],
                $params
            )
        );
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

}