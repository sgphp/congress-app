<?php


namespace  Application\Service\Civic\Thread;

use Application\Model\Division;
use Application\Model\Office;
use Application\Model\Official;
use Application\Module;

/**
 * Class StateParser
 * @package Application\Service\Civic\Thread
 */
class StateParser
{

    const BATCH_SIZE = 400;

    /**
     * @var array
     */
    private $results;

    /**
     * @param array $results
     */
    public function pushResults(array $results)
    {
        $this->results = $results;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $count = 0;

        vprintf(
            "Start parsing state.\nDivisions\t%d\nOffices\t%d\nOfficials\t%d\n",
            [
                count($this->results['divisions']),
                count($this->results['offices']),
                count($this->results['officials']),
            ]
        );

        foreach ($this->results['divisions'] as $id => $division) {

            $divisionModel = Module::entityManager()
                ->getRepository(Division::class)
                ->findOneBy(
                    [
                        'name'  => $division['name'],
                        'ocdId' => $id
                    ]
                );

            if (null === $divisionModel) {
                $divisionModel = new Division();
                $divisionModel->setName($division['name']);
                $divisionModel->setOcdId($id);
            }

            Module::entityManager()->persist($divisionModel);
            if (++$count % static::BATCH_SIZE === 0) {
                Module::entityManager()->flush();
                Module::entityManager()->clear();
            }

            foreach ($division['officeIndices'] ?? [] as $officeIndex) {
                $office = $this->results['offices'][$officeIndex];

                $officeModel = Module::entityManager()
                    ->getRepository(Office::class)
                    ->findOneBy(
                        [
                            'name'     => $office['name'],
                            'division' => $divisionModel
                        ]
                    );

                if (null === $officeModel) {
                    $officeModel = new Office();
                    $officeModel->setName($office['name']);
                    $officeModel->setDivision($divisionModel);
                    $officeModel->setLevels($office['levels'] ?? []);
                    $officeModel->setRoles($office['roles'] ?? []);

                    Module::entityManager()->persist($officeModel);
                }

                foreach ($office['officialIndices'] ?? [] as $officialIndex) {
                    $official = $this->results['officials'][$officialIndex];

                    $officialModel = Module::entityManager()
                        ->getRepository(Official::class)
                        ->findOneBy(
                            [
                                'name'     => $official['name'],
                                'office'   => $officeModel
                            ]
                        );

                    if (null === $officialModel) {
                        $officialModel = new Official();
                        $officialModel->setName($official['name']);
                        $officialModel->setParty($official['party'] ?? 'Undefined');
                        $officialModel->setAddress($official['address'] ?? []);
                        $officialModel->setEmails($official['emails'] ?? []);
                        $officialModel->setPhones($official['phones'] ?? []);
                        $officialModel->setUrls($official['urls'] ?? []);
                        $officialModel->setPhotoUrl($official['photoUrl'] ?? null);
                        $officialModel->setChannels($official['channels'] ?? []);
                        $officialModel->setOffice($officeModel);

                        Module::entityManager()->persist($officialModel);
                    }

                    unset($this->results['officials'][$officialIndex]);
                }

                unset($this->results['offices'][$officeIndex]);
            }
        }

        Module::entityManager()->flush();
        Module::entityManager()->clear();

        printf("Memory usage\t%0.2f %s\n", memory_get_usage() / 1024 / 1024, 'MB');
    }
}