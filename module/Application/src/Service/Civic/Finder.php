<?php

namespace Application\Service\Civic;

use Application\Model\Division;
use Application\Model\Office;
use Application\Model\Official;
use Application\Module;
use Doctrine\Common\Collections\Criteria;

/**
 * Class Finder
 * @package Application\Service\Civic
 */
class Finder
{

    /**
     * @param array $divisionsIds
     * @return Official[]
     */
    public static function findOfficialsByDivisions(array $divisionsIds)
    {
        $divisionsCriteria = new Criteria();
        $divisionsCriteria->where($divisionsCriteria->expr()->in('ocdId', $divisionsIds));

        $divisions = Module::entityManager()->getRepository(Division::class)->matching($divisionsCriteria)->toArray();

        $officesCriteria = new Criteria();
        $officesCriteria->where($divisionsCriteria->expr()->in('division', $divisions));

        $offices = Module::entityManager()->getRepository(Office::class)->matching($officesCriteria)->toArray();

        $officialsCriteria = new Criteria();
        $officialsCriteria->where($divisionsCriteria->expr()->in('office', $offices));

        $officials = Module::entityManager()->getRepository(Official::class)->matching($officialsCriteria)->toArray();

        return $officials;
    }

}