<?php

namespace Application\Service\Letter;

use Application\Model\Official;
use Application\Model\Template;
use Application\Model\User;
use Application\Service\Letter\Configuration\ConfigurationAbstract;
use Application\Service\Letter\Configuration\Legislator;
use Application\Service\Letter\Configuration\User as UserConfiguration;

/**
 * Class ContentManager
 * @package Application\Service\Letter
 */
class ContentManager
{

    const CONFIGURATION = [
        Legislator::class,
        UserConfiguration::class,
    ];

    /**
     * @var Template
     */
    private $template;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Official
     */
    private $legislator;

    /**
     * @var $content
     */
    private $content;

    /**
     * LetterContent constructor.
     * @param  Template  $template
     * @param  Official  $legislator
     * @param  User      $user
     */
    public function __construct(Template $template, $legislator, $user)
    {
        $this->template = $template;
        $this->user = $user;
        $this->legislator = $legislator;
        $this->content = $template->getTemplate();

    }

    /**
     * @return string
     */
    public function getContent()
    {
        $replaceData = [];

        foreach (static::CONFIGURATION as $configurationClass) {
            if (false === class_exists($configurationClass)) {
                continue;
            }

            if (false === property_exists($this, $configurationClass::OBJECT_NAME)) {
                continue;
            }

            if (null === $this->{$configurationClass::OBJECT_NAME}) {
                continue;
            }

            /** @var ConfigurationAbstract $configuration */
            $configuration = new $configurationClass($this->{$configurationClass::OBJECT_NAME});

            foreach ($configuration->getData() as $replace) {

                if (true === isset($replace[ConfigurationAbstract::REPLACE_CONDITION])) {

                    if ($replace[ConfigurationAbstract::REPLACE_CONDITION]) {
                        $replaceData[$replace[ConfigurationAbstract::REPLACE_VALUE]] = $replace[ConfigurationAbstract::REPLACE_IF_TRUE];
                    } elseif (true === isset($replace[ConfigurationAbstract::REPLACE_IF_FALSE])) {
                        $replaceData[$replace[ConfigurationAbstract::REPLACE_VALUE]] = $replace[ConfigurationAbstract::REPLACE_IF_FALSE];
                    }
                } else {
                    $replaceData[$replace[ConfigurationAbstract::REPLACE_VALUE]] = $replace[ConfigurationAbstract::REPLACE_IF_TRUE];
                }
            }
        }

        $this->content = str_replace(array_keys($replaceData), array_values($replaceData), $this->content);

        return $this->content;
    }

}