<?php

namespace Application\Service\Letter\Configuration;

use Application\Model\AbstractModel\Concern\ConstantReflector;
use Application\Model\Official;

/**
 * Class Legislator
 * @package Application\Service\Letter\Configuration
 * @method Official getObject()
 */
class Legislator extends ConfigurationAbstract
{

    use ConstantReflector;

    const OBJECT_NAME = 'legislator';

    const LEGISLATOR_ADDRESS = 'Legislator Address';
    const LEGISLATOR_ZIP     = 'Legislator ZIP';
    const LEGISLATOR_NAME    = 'Legislator Name';

    /**
     * @var Official
     */
    protected $object;

    /**
     *
     */
    public function getData()
    {
        return [
            [
                static::REPLACE_VALUE     => $this->shortcode(static::LEGISLATOR_ADDRESS),
                static::REPLACE_CONDITION => true === isset($this->object->getAddress()[0]),
                static::REPLACE_IF_TRUE   => @implode(' ', $this->object->getAddress()[0]),
                static::REPLACE_IF_FALSE  => '[' . static::LEGISLATOR_ADDRESS . ']'
            ],
            [
                static::REPLACE_VALUE     => $this->shortcode(static::LEGISLATOR_ZIP),
                static::REPLACE_CONDITION => true === isset($this->object->getAddress()[0]['zip']),
                static::REPLACE_IF_TRUE   => @$this->object->getAddress()[0]['zip'],
                static::REPLACE_IF_FALSE  => '[' . static::LEGISLATOR_ZIP . ']'
            ],
            [
                static::REPLACE_VALUE     => $this->shortcode(static::LEGISLATOR_NAME),
                static::REPLACE_IF_TRUE   => $this->object->getName(),
            ]
        ];
    }


}