<?php

namespace Application\Service\Letter\Configuration;

/**
 * Interface ConfigurationInterface
 * @package Application\Service\Letter\Configuration
 */
abstract class ConfigurationAbstract
{

    const OBJECT_NAME = 'object';

    /**
     * Replace keys of one piece of data
     */
    const REPLACE_VALUE     = 'replace_value';
    const REPLACE_CONDITION = 'replace_condition';
    const REPLACE_IF_TRUE   = 'replace_if_true';
    const REPLACE_IF_FALSE  = 'replace_if_false';

    /**
     * @var object
     */
    protected $object;

    /**
     * ConfigurationAbstract constructor.
     * @param object $object
     */
    public function __construct($object)
    {
        $this->object = $object;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param object $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @param $value
     * @return string
     */
    protected function shortcode($value)
    {
        return '&lt;&lt;&lt;' . $value . '&gt;&gt;&gt;';
    }

    /**
     * @return array Associative array in format [Replace constant => Replace value]
     */
    abstract public function getData();

}