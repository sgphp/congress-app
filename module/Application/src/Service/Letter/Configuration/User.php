<?php

namespace Application\Service\Letter\Configuration;

use Application\Model\AbstractModel\Concern\ConstantReflector;
use Application\Model\User as UserModel;

/**
 * Class User
 * @package Application\Service\Letter\Configuration
 */
class User extends ConfigurationAbstract
{

    use ConstantReflector;

    const OBJECT_NAME = 'user';

    const USER_EMAIL         = 'Sender email';
    const USER_SIGNATURE     = 'Sender signature';
    const USER_ZIP           = 'Sender ZIP';
    const USER_NAME          = 'Sender Name';
    const USER_ADDRESS_LINE  = 'Sender Address Line';
    const USER_ANSWERS_PLACE = 'Answers place';

    /**
     * @var UserModel
     */
    protected $object;

    /**
     * @return array
     */
    public function getData()
    {
        return [
            [
                static::REPLACE_VALUE   => $this->shortcode(static::USER_ADDRESS_LINE),
                static::REPLACE_IF_TRUE => $this->object->getSettings()->getAddressLine()
            ],
            [
                static::REPLACE_VALUE   => $this->shortcode(static::USER_NAME),
                static::REPLACE_IF_TRUE => $this->object->getName()
            ],
            [
                static::REPLACE_VALUE   => $this->shortcode(static::USER_ZIP),
                static::REPLACE_IF_TRUE => $this->object->getSettings()->getZip()
            ],
            [
                static::REPLACE_VALUE   => $this->shortcode(static::USER_EMAIL),
                static::REPLACE_IF_TRUE => $this->object->getEmail()
            ],
            [
                static::REPLACE_VALUE   => $this->shortcode(static::USER_SIGNATURE),
                static::REPLACE_IF_TRUE =>
                    '' !== $this->object->getSignatureImage()->getValue()
                        ? vsprintf(
                            '<img src="%s" width="200px">',
                            [
                                $this->object->getSignatureImage()->getValue()
                            ]
                        )
                        : vsprintf(
                            '<span style="font-family: %s;font-size: 16px">%s</span>',
                            [
                                $this->object->getSignature()->getFont(),
                                $this->object->getSignature()->getValue(),
                            ]
                        )

            ],
        ];
    }


}