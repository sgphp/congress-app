<?php

namespace Application\Form;

use Application\Form\Element\Email;
use Application\Form\Element\Name;
use Application\Form\Element\Password;
use Zend\Form\Form;

/**
 * Class Register
 * @package Application\Form
 */
class Register extends Form
{

    /**
     * Register constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct(null, $options);

        $this->add(
            [
                'type' => Email::class,
                'name' => 'email',
            ]
        );

        $this->add(
            [
                'type' => Name::class,
                'name' => 'name',
            ]
        );

        $this->add(
            [
                'type' => Password::class,
                'name' => 'password',
                'options' => [
                    'email'         => $this->getOption('email'),
                    'entityManager' => $this->getOption('entityManager'),
                    'check'         => \Application\Form\Validator\Password::CHECK_REGISTER
                ]
            ]
        );
        
        $this->add(
            [
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'accept',
                'options' => [
                    'checked_value' => 1,
                    'unchecked_value' => null,
                ]
            ]
        );
    }

}