<?php

namespace Application\Form;

use Application\Form\Element\PasswordLength;
use Zend\Form\Element\Text;
use Zend\Form\Form;

/**
 * Class Login
 * @package Application\Form
 */
class RecoverPassword extends Form
{

    /**
     * Login constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        parent::__construct(null, $options);

        $this->add(
            [
                'type' => PasswordLength::class,
                'name' => 'password',
            ]
        );
    }

}