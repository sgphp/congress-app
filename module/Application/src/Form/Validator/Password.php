<?php

namespace Application\Form\Validator;

use Application\Form\Login;
use Application\Model\User;
use Application\Module;
use Zend\Validator\AbstractValidator;

/**
 * Class Password
 * @package Application\Form\Validator
 */
class Password extends AbstractValidator
{

    /**
     * Validator check constants
     */
    const CHECK_LOGIN    = 'login';
    const CHECK_REGISTER = 'password';

    /**
     * Messages keys
     */
    const FOUND     = 'found';
    const NOT_FOUND = 'not_found';
    const EXCEPTION = 'exception';

    /**
     * Messages templates
     * @var array
     */
    protected $messageTemplates = [
        self::FOUND     => /*translate*/'User with same email found'/*translate*/,
        self::NOT_FOUND => /*translate*/'Invalid email or/and password'/*translate*/,
        self::EXCEPTION => /*translate*/'Some error occurred while logging in'/*translate*/,
    ];

    /**
     * @param string $value
     * @return bool
     */
    public function isValid($value)
    {
        $result = false;

        try {

            $email = $this->getOption('email');
            $check = $this->getOption('check');

            $repository = Module::entityManager()->getRepository(User::class);

            $criteria = [
                'email' => $email,
            ];

            if ($check === static::CHECK_LOGIN) {
                $criteria['password'] = User::hashPassword($value);
            }

            $user = $repository->findOneBy($criteria);

            switch (true) {
                case $user !== null && $check === static::CHECK_LOGIN:
                    /** @var Login $form */
                    $form = $this->getOption('form');
                    $form->setIdentity($user->getId());
                    $result = true;
                    break;
                case $user === null && $check === static::CHECK_REGISTER:
                    $result = true;
                    break;
                case $user === null && $check === static::CHECK_LOGIN:
                    $this->error(static::NOT_FOUND);
                    break;
                case $user !== null && $check === static::CHECK_REGISTER:
                    $this->error(static::FOUND);
                    break;
            }

        } catch (\Exception $exception) {
            $this->error(static::EXCEPTION);
        }

        return $result;
    }

}