<?php

namespace Application\Controller;

use Application\Model\Letter;
use Application\Model\LetterCount;
use Application\Model\Transaction;
use Application\Module;
use Zend\View\Model\ViewModel;

/**
 * Class LetterIpnController
 * @package Application\Controller
 */
class LetterIpnController extends AbstractController
{

    public function init()
    {
        $this->layout('layout/letter');
    }

    /**
     * @return array|ViewModel
     */
    public function successAction()
    {
        $date = new \DateTime();

        $letterCount = $this->getEntityManager()
            ->getRepository(LetterCount::class)
            ->findOneBy(
                [
                    'week'  => $date->format('W'),
                    'year'  => $date->format('Y')
                ]
            );

        if (null === $letterCount) {
            $letterCount = new LetterCount();
            $letterCount->setCount(1);
            $letterCount->setYear($date->format('Y'));
            $letterCount->setWeek($date->format('W'));
        } else {
            $letterCount->setCount($letterCount->getCount() + 1);
        }

        $this->getEntityManager()->persist($letterCount);
        $this->getEntityManager()->flush($letterCount);

        $transaction = $this->getTransaction();

        if (null === $transaction) {
            return $this->notFoundAction();
        }

        /** @var Letter $letter */
        $letter = $this->getEntityManager()
            ->getRepository(Letter::class)
            ->findOneBy(
                [
                    'transaction' => $transaction
                ]
            );

        if (null === $transaction) {
            return $this->notFoundAction();
        }

        // TODO Send confirmation email Module::getMailSender()->sendMail();

        Module::getMailSender()->sendMail(
            'Letter ' . $letter->getTemplate()->getName() . ' was created by ' .  ($letter->getUser() ? $letter->getUser()->getName() : 'Guest'),
            $letter->getResponsibleEmail(),
            'admin/letter-created',
            [
                'letter' => $letter
            ]
        );

        $letter->setStatus(Letter::STATUS_QUEUE);

        $transaction->setStatus(Transaction::STATUS_SUCCESS);

        $this->getEntityManager()->persist($letter);
        $this->getEntityManager()->persist($transaction);
        $this->getEntityManager()->flush();

        return new ViewModel(
            [
                'letter' => $letter,
                'user'   => $this->getUser()
            ]
        );
    }

    /**
     * @return array|ViewModel
     */
    public function cancelAction()
    {
        $transaction = $this->getTransaction();

        if (null === $transaction) {
            return $this->notFoundAction();
        }

        $transaction->setStatus(Transaction::STATUS_CANCEL);

        $this->getEntityManager()->persist($transaction);
        $this->getEntityManager()->flush($transaction);

        return new ViewModel();

    }

    /**
     * @return null|Transaction
     */
    protected function getTransaction()
    {
        /** @var Transaction $transaction */
        $transaction = $this->getEntityManager()
            ->getRepository(Transaction::class)
            ->findOneBy(
                [
                    'id'        => $this->params('transaction'),
                    'paymentId' => $this->getRequest()->getQuery('paymentId'),
                    'status'    => Transaction::STATUS_WAIT
                ]
            );

        return $transaction;
    }

}