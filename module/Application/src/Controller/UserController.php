<?php

namespace Application\Controller;

use Application\Form\ForgotPassword;
use Application\Form\Login;
use Application\Form\RecoverPassword;
use Application\Form\Register;
use Application\Model\Settings;
use Application\Model\Signature;
use Application\Model\SignatureImage;
use Application\Model\User;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\Model\PasswordRecovery;
use Application\Module;

/**
 * Class UserController
 * @package Application\Controller
 */
class UserController extends AbstractController
{

    public function restrictLoggedIn()
    {
        if ($this->getUser() !== null) {
            $this->redirect()->toRoute('home');
        }
    }

    /**
     * Login action
     * @return ViewModel
     */
    public function loginAction()
    {
        $this->restrictLoggedIn();

        /** @var Request $request */
        $request = $this->getRequest();

        if (true === $request->isPost() && true === $request->isXmlHttpRequest()) {

            $json = new JsonModel();

            $form = new Login(
                [
                    'email' => $request->getPost('email'),
                ]
            );

            $data = $request->getPost();

            if ($form->setData($data)->isValid() === true) {
                $json->setVariable('redirect', $this->url()->fromRoute('home'));
                $this->getAuth()->getStorage()->write($form->getIdentity());
            } else {
                $json->setVariable('errors', $form->getMessages());
            }

            return $json;
        }

        return new ViewModel();
    }

    /**
     * @return ViewModel|array
     */
    public function registerAction()
    {
        $this->restrictLoggedIn();

        /** @var Request $request */
        $request = $this->getRequest();

        if (true === $request->isPost() && true === $request->isXmlHttpRequest()) {

            $json = new JsonModel();

            $form = new Register(
                [
                    'email'         => $request->getPost('email'),
                    'entityManager' => $this->getEntityManager()
                ]
            );

            $data = $request->getPost();

            if ($form->setData($data)->isValid() === true) {


                $settings = new Settings();
                $settings->setAddressLine('')
                    ->setCity('')
                    ->setState('')
                    ->setZip('');

                $this->getEntityManager()->persist($settings);
                $this->getEntityManager()->flush($settings);
                
                $signature = new Signature();
                $signature->setPicture('')
                    ->setType('')
                    ->setFont('')
                    ->setValue('');

                $signatureImage = new SignatureImage();
                $signatureImage->setValue('');

                $this->getEntityManager()->persist($signature);
                $this->getEntityManager()->persist($signatureImage);
                $this->getEntityManager()->flush();

                $user = new User();
                $user->setEmail($form->get('email')->getValue());
                $user->setName($form->get('name')->getValue());
                $user->setPassword(User::hashPassword($form->get('password')->getValue()));
                $user->setRole(User::ROLE_USER);
                $user->setSettings($settings);
                $user->setSignature($signature);
                $user->setSignatureImage($signatureImage);
                $user->setCreated(new \DateTime());

                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();

                $this->getAuth()->getStorage()->write($user->getId());

                $json->setVariables(
                    [
                        'redirect' => $this->url()->fromRoute('user', ['action' => 'settings']),
                        'message'  => 'Successfully registered'
                    ]
                );
            } else {
                $json->setVariable('errors', $form->getMessages());
            }

            return $json;
        }

        if (null === $this->getRequest()->getQuery('modal')) {
            return new ViewModel();
        } else {
            return new JsonModel(
                [
                    'modal' => $this->getRenderer()->render(
                        'layout/concern/modal',
                        [
                            'title' => 'Register new account',
                            'modal' => $this->getRenderer()->render(
                                'layout/concern/register-box'
                            )
                        ]
                    )
                ]
            );
        }
    }

    /**
     * @return void
     */
    public function logoutAction()
    {
        $this->getAuth()
            ->getStorage()
            ->clear();

        $this->redirect()->toRoute('user', ['action' => 'login']);
    }

    /**
     * @return JsonModel|ViewModel
     */
    public function forgotPasswordAction()
    {
        if (true === $this->getRequest()->isPost()) {
            $result = new JsonModel();

            $form = new ForgotPassword();
            $form->setData($this->getRequest()->getPost()->toArray());

            if (true === $form->isValid()) {
                /** @var User $user */
                $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => $form->get('email')->getValue()]);

                $recovery = new PasswordRecovery();
                $recovery->setUser($user);
                $recovery->setHash(PasswordRecovery::generateHash());
                $recovery->setActive(true);

                $this->getEntityManager()->persist($recovery);
                $this->getEntityManager()->flush($recovery);

                Module::getMailSender()->sendMail(
                   'Password recovery',
                    $user->getEmail(),
                    'user/forgot-password',
                    [
                        'email' => $user->getEmail(),
                        'hash'  => $recovery->getHash()
                    ]
                );

                $result->setVariable('redirect', $this->url()->fromRoute('index', ['action' => 'information']));

            } else {
                $result->setVariable('errors', $form->getMessages());
            }

            return $result;
        }

        return new ViewModel();
    }

    /**
     * @return array|JsonModel|ViewModel
     */
    public function recoverPasswordAction()
    {
        if (true === $this->getRequest()->isPost()) {
            $form = new RecoverPassword();
            $form->setData($this->getRequest()->getPost()->toArray());

            $result = new JsonModel();

            if (false === $form->isValid()) {
                $result->setVariable('errors', $form->getMessages());
            } else {
                /** @var PasswordRecovery $recoverModel */
                $recoverModel = $this->getEntityManager()
                    ->getRepository(PasswordRecovery::class)
                    ->findOneBy(
                        [
                            'hash' => $this->getRequest()->getPost('hash'),
                            'active' => true
                        ]
                    );

                if ($recoverModel === null) {
                    $result->setVariable('errors', ['password' => ['Recovery password link is not valid']]);
                } else {
                    $recoverModel->setActive(false);
                    $this->getEntityManager()->persist($recoverModel);

                    $user = $recoverModel->getUser();
                    $user->setPassword(User::hashPassword($form->get('password')->getValue()));

                    $this->getEntityManager()->flush();

                    $result->setVariable('redirect', $this->url()->fromRoute('user', ['action' => 'login']));
                }
            }

            return $result;

        } else {
            $hash = $this->params('hash');
            $view = new ViewModel();

            if (true === empty($hash)) {
                return $this->notFoundAction();
            }

            /** @var PasswordRecovery $recoverModel */
            $recoverModel = $this->getEntityManager()
                ->getRepository(PasswordRecovery::class)
                ->findOneBy(
                    [
                        'hash' => $hash,
                        'active' => true
                    ]
                );

            if (null === $recoverModel) {
                return $this->notFoundAction();
            }

            $view->setVariable('hash', $hash);

            return $view;
        }
    }

    /**
     * @return \Zend\Http\Response
     */
    public function recoverPasswordCancelAction()
    {
        /** @var PasswordRecovery $recoverModel */
        $recoverModel = $this->getEntityManager()
            ->getRepository(PasswordRecovery::class)
            ->findOneBy(
                [
                    'hash' => $this->params('hash'),
                    'active' => true
                ]
            );

        if ($recoverModel !== null) {
            $recoverModel->setActive(false);
            $this->getEntityManager()->persist($recoverModel);
            $this->getEntityManager()->flush();
        }

        return $this->redirect()->toRoute('home');
    }

    /**
     * @return array|JsonModel|ViewModel
     */
    public function settingsAction()
    {
        $id = $this->getUser()->getId();

            if ($id !== null && true == $this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()) {
                /** @var User $user */
                $user = $this->getEntityManager()
                    ->getRepository(User::class)
                    ->findOneBy(
                        [
                            'id' => $id
                        ]
                    );
                $user->setName($this->getRequest()->getPost('name'));
                $user->getSettings()->setAddressLine($this->getRequest()->getPost('address'));
                $user->getSettings()->setCity($this->getRequest()->getPost('city'));
                $user->getSettings()->setState($this->getRequest()->getPost('state'));
                $user->getSettings()->setZip($this->getRequest()->getPost('zip'));
                $user->getSignature()->setValue($this->getRequest()->getPost('signature'));
                $user->getSignature()->setFont($this->getRequest()->getPost('font'));
                $user->getSignatureImage()->setValue($this->getRequest()->getPost('signature_image'));
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();
                return new JsonModel(
                    [
                        'redirect' => $this->url()->fromRoute('user', ['action' => 'settings']),
                    ]);
            } else {
                if (null !== $id ) {
                    $user = $this->getEntityManager()
                        ->getRepository(User::class)
                        ->findOneBy(['id' => $id]);

                    $config = $this->getEvent()
                        ->getApplication()
                        ->getServiceManager()
                        ->get('config');

                    if (null !== $user) {
                        $view = new ViewModel(
                            [
                                'user' => $user,
                                'states' => $config['states'] ?? [],
                                'action'  => $this->url()->fromRoute('user', ['action' => 'settings'])
                            ]
                        );

                        return $view;
                    }
                }
                return $this->notFoundAction();
            }

    }

    /**
     * @return ViewModel
     */
    public function showAction()
    {
        $user = $this->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy(
            [
                'id' => $this->getUser()->getId()

            ]
        );
            return new ViewModel(
                [
                    'user' => $user,
                ]
            );
    }

}