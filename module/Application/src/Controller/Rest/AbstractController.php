<?php

namespace Application\Controller\Rest;
use Zend\Stdlib\ArrayUtils;
use Zend\View\Model\JsonModel;

/**
 * Class AbstractController
 * @package Application\Controller\Rest
 */
abstract class AbstractController extends \Application\Controller\AbstractController
{

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    protected function attach($key, $value = null)
    {
        if (true === is_array($key)) {
            $this->params = ArrayUtils::merge($this->params, $key);
        } else {
            $this->params[$key] = $value;
        }

        return $this;
    }

    /**
     * @return JsonModel
     */
    protected function result()
    {
        $result = new JsonModel();
        $result->setVariables($this->params);
        return $result;
    }

}