<?php

namespace Application\Controller\Rest;

use Application\Service\Civic\Api;
use Application\Service\Civic\Finder;
use GuzzleHttp\Exception\RequestException;
use Zend\Json\Json;

/**
 * Class CivilApiController
 * @package Application\Controller\Rest
 */
class CivicApiController extends AbstractController
{

    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function representativeInfoAction()
    {
        $api = $this->api()->getApi();

        try {
            $message = $api->request(
                Api::METHOD_REPRESENTATIVE_INFO,
                [
                    'address' => $this->getRequest()->getPost('address', '')
                ]
            );

            $result = Json::decode($message->getBody()->getContents(), Json::TYPE_ARRAY);

            $html = $this->getRenderer()->render(
                'layout/concern/officials',
                [
                    'officials' => Finder::findOfficialsByDivisions(array_keys($result['divisions']))
                ]
            );
            $this->attach('html', $html);

            if (null !== $this->getUser()) {
                $settings = $this->getUser()->getSettings();

                if ('' === $settings->getAddressLine()) {
                    $settings->setAddressLine($this->getRequest()->getPost('street', ''));
                }

                if ('' === $settings->getCity()) {
                    $settings->setCity($this->getRequest()->getPost('city', ''));
                }

                if ('' === $settings->getZip()) {
                    $settings->setZip($this->getRequest()->getPost('zip', ''));
                }

                if ('' === $settings->getState()) {
                    $settings->setState($this->getRequest()->getPost('state', ''));
                }

                $this->getEntityManager()->persist($settings);
                $this->getEntityManager()->flush($settings);
            }

        } catch (RequestException $requestException) {
            $this->attach([
                'error' => $requestException->getMessage(),
            ]);
        }

        return $this->result();
    }

}