<?php

namespace Application\Controller\Rest;

use Application\Model\Division;
use Application\Model\Office;
use Application\Model\Official;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\ExpressionBuilder;
use Zend\View\Model\JsonModel;

/**
 * Class ServiceController
 * @package Application\Controller\Rest
 */
class ServiceController extends AbstractController
{

    /**
     * @return JsonModel
     */
    public function headerMenuAction()
    {
        $menuListTemplate = ($this->getUser() === null) ? 'guest' : $this->getUser()->getRole();

        return new JsonModel(
            [
                'menu-right-li' => $this->getRenderer()->render('layout/menu/' . $menuListTemplate)
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function byDivisionsAction()
    {
        $mapFunction = function ($division) {
            /** @var Division $division */
            return [
                'id'    => $division->getId(),
                'text'  => $division->getName() . ' ' . preg_replace('/.*country:us\/state:(\w{2}).*/', '($1)', $division->getOcdId()),
            ];
        };

        $expressions = [
            (new ExpressionBuilder())->contains('name', $this->getRequest()->getQuery('term')),
            (new ExpressionBuilder())->contains('ocdId', $this->getRequest()->getQuery('term')),
        ];

        return new JsonModel(
            [
                'values' => $this->getMatchingValues($expressions, Division::class, $mapFunction)
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function byOfficesAction()
    {
        $mapFunction = function ($office) {
            /** @var Office $office */
            return [
                'id'    => $office->getId(),
                'text'  => $office->getName() . ' ' . preg_replace('/.*country:us\/state:(\w{2}).*/', '($1)', $office->getDivision()->getOcdId()),
            ];
        };

        $expression = [(new ExpressionBuilder())->contains('name', $this->getRequest()->getQuery('term'))];

        return new JsonModel(
            [
                'values' => $this->getMatchingValues($expression, Office::class, $mapFunction)
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function byOfficialsAction()
    {
        $mapFunction = function ($official) {
            /** @var Official $official */
            return [
                'id'    => $official->getId(),
                'text'  => $official->getName() . ' ' . preg_replace('/.*country:us\/state:(\w{2}).*/', '($1)', $official->getOffice()->getDivision()->getOcdId()),
            ];
        };

        $expression = [(new ExpressionBuilder())->contains('name', $this->getRequest()->getQuery('term'))];

        return new JsonModel(
            [
                'values' => $this->getMatchingValues($expression, Official::class, $mapFunction)
            ]
        );
    }

    public function byOfficialsRenderAction()
    {
        $mapFunction = function ($official) {
            /** @var Official $official */
            return [
                'id'    => $official->getId(),
                'text'  => $this->getRenderer()->render('layout/concern/official-tiny', ['official' => $official])
            ];
        };

        $expression = [(new ExpressionBuilder())->contains('name', $this->getRequest()->getQuery('term'))];

        return new JsonModel(
            [
                'values' => $this->getMatchingValues($expression, Official::class, $mapFunction)
            ]
        );
    }

    /**
     * @param Comparison[]|Comparison $expressions
     * @param string                  $entity
     * @param callable                $mapFunction
     *
     * @return array
     */
    protected function getMatchingValues($expressions, string $entity, callable $mapFunction)
    {
        $criteria = new Criteria();

        foreach ($expressions as $expression) {
            $criteria->orWhere($expression);
        }

        $criteria->orderBy(
            [
                'name' => Criteria::ASC,
            ]
        );

        $criteria->setMaxResults(30);

        $values = $this->getEntityManager()->getRepository($entity)->matching($criteria);
        return $values->map($mapFunction)->toArray();
    }

}