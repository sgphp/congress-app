<?php

namespace Application\Controller;

use Application\Model\Category;
use Application\Model\Letter;
use Application\Model\LetterCount;
use Application\Model\Office;
use Application\Model\Official;
use Application\Model\Question;
use Application\Model\Tag;
use Application\Model\Template;
use Application\Model\TemplatePipeline;
use Application\Model\TemplateTag;
use Application\Model\Transaction;
use Application\Model\Variant;
use Application\Service\Civic\Api;
use Application\Service\Civic\Finder;
use Application\Service\Letter\ContentManager;
use Application\Service\Options;
use Doctrine\Common\Collections\Criteria;
use Dompdf\Dompdf;
use PayPal\Api\Payment;
use PayPal\Exception\PayPalConnectionException;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class LetterController
 *
 * @package Application\Controller
 */
class LetterController extends AbstractController
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->layout('layout/letter');
    }

    /**
     * An index action
     * @return ViewModel
     */
    public function indexAction()
    {
        $config = $this->getEvent()
            ->getApplication()
            ->getServiceManager()
            ->get('config');

        $view = new ViewModel(
            [
                'states'   => $config['states'] ?? [],
                'settings' => $this->getUser() === null ? null : $this->getUser()->getSettings(),
                'user'     => $this->getUser()
            ]
        );

        return $view;
    }

    /**
     * @return ViewModel
     */
    public function browseAction()
    {
        return new ViewModel(
            [
                'categories' => $this->getEntityManager()->getRepository(Category::class)->findAll(),
                'tags' => $this->getEntityManager()->getRepository(Tag::class)->findBy([], ['count' => Criteria::DESC], 30),
            ]
        );
    }

    /**
     * @return array|ViewModel
     */
    public function categoryLettersAction()
    {
        $category = $this->getEntityManager()
            ->getRepository(Category::class)
            ->findOneBy(
                [
                    'slug' => $this->params('category')
                ]
            );

        if (null === $category) {
            return $this->notFoundAction();
        }

        $templates = $this->getEntityManager()
            ->getRepository(Template::class)
            ->findBy(
                [
                    'category' => $category
                ]
            );

        $view = new ViewModel(
            [
                'header'    => $this->getRenderer()->render('layout/concern/category-message', ['category' => $category]),
                'templates' => array_filter($templates, function ($template) {
                    /** @var Template $template */
                    return $template->isEnabled();
                })
            ]
        );

        $view->setTemplate('application/letter/compose');

        return $view;
    }

    /**
     * @return array|ViewModel
     */
    public function tagLettersAction()
    {
        $tag = $this->getEntityManager()
            ->getRepository(Tag::class)
            ->find($this->params('tag'));

        if (null === $tag) {
            return $this->notFoundAction();
        }

        $templateTags = $this->getEntityManager()
            ->getRepository(TemplateTag::class)
            ->findBy(
                [
                    'tag' => $tag
                ]
            );

        $templates = array_map(
            function ($templateTag) {
                /** @var TemplateTag $templateTag */
                return $templateTag->getTemplate();
            },
            $templateTags
        );

        $view = new ViewModel(
            [
                'header'    => $this->getRenderer()->render('layout/concern/tag-message', ['tag' => $tag]),
                'templates' => array_filter($templates, function ($template) {
                    /** @var Template $template */
                    return $template->isEnabled();
                })
            ]
        );

        $view->setTemplate('application/letter/compose');

        return $view;
    }

    /**
     * @return ViewModel
     */
    public function composeAction()
    {
        $official = urldecode($this->params('official'));

        /** @var \Application\Model\Official $officialModel */
        $officialModel = $this->getEntityManager()->getRepository(Official::class)->find($official);

        $searchExact = [
            'division' => $officialModel->getOffice()->getDivision(),
            'official' => $official,
        ];

        $searchDivision = [
            'division' => $officialModel->getOffice()->getDivision(),
            'office'   => null,
            'official' => null,
        ];

        $searchOffice = [
            'division' => $officialModel->getOffice()->getDivision(),
            'office'   => $officialModel->getOffice(),
            'official' => null,
        ];

        $searchEmpty = [
            'division' => null,
            'office'   => null,
            'official' => null,
        ];

        $pipelinesRepository = $this->getEntityManager()->getRepository(TemplatePipeline::class);

        $pipelines = array_merge(
            $pipelinesRepository->findBy($searchExact),
            $pipelinesRepository->findBy($searchDivision),
            $pipelinesRepository->findBy($searchOffice),
            $pipelinesRepository->findBy($searchEmpty)
        );

        $templates = array_map(
            function ($pipeline) {
                /** @var TemplatePipeline $pipeline */
                return $pipeline->getTemplate();
            },
            $pipelines
        );

        usort(
            $templates,
            function ($first, $second) {
                /** @var Template $first */
                /** @var Template $second */

                return $second->getCreated() <=> $first->getCreated();
            }
        );

        $templates = array_filter($templates, function ($template) {
            /** @var Template $template */
            return $template->isEnabled();
        });

        $templateIds = array_map(
            function ($template) {
                /** @var Template $template */
                return $template->getId();
            },
            $templates
        );

        $criteria = new Criteria();
        $criteria->where($criteria->expr()->notIn('id', $templateIds));
        $criteria->andWhere($criteria->expr()->eq('enabled', true));

        $allTemplates = $this
            ->getEntityManager()
            ->getRepository(Template::class)
            ->matching($criteria);

        return new ViewModel(
            [
                'templates' => $templates,
                'allTemplates' => $allTemplates
            ]
        );
    }

    /**
     * @return array|\Zend\Http\Response|JsonModel|ViewModel
     */
    public function composeTemplateAction()
    {
        $officialId = $this->params('official');

        if (null === $officialId) {
            return $this->redirect()->toRoute('choose-letter-template-legislator', ['template' => $this->params('template')]);
        }

        $official = null;

        if (null !== $officialId) {
            /** @var \Application\Model\Official $official */
            $official = $this->getEntityManager()->getRepository(Official::class)->find($officialId);

            if (null === $official) {
                return $this->notFoundAction();
            }
        }

        /** @var Template $template */
        $template = $this->getEntityManager()->getRepository(Template::class)->find($this->params('template'));

        if (null === $template) {
            return $this->notFoundAction();
        }

        $contentManager = new ContentManager($template, $official, $this->getUser());
        $content = $contentManager->getContent();

        return new ViewModel(
            [
                'official' => $official,
                'template' => $template,
                'content'  => $content
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function questionsAction()
    {

        $template = $this->getEntityManager()
            ->getRepository(Template::class)
            ->find(
                $this->getRequest()->getPost('template')
            );

        $questions = $this->getEntityManager()
            ->getRepository(Question::class)
            ->findBy(
                [
                    'template' => $template
                ]
            );

        $criteria = new Criteria();
        $criteria->where($criteria->expr()->in('question', $questions));

        $variants = $this->getEntityManager()
            ->getRepository(Variant::class)
            ->matching($criteria);

        return new JsonModel(
            empty($questions) ?
                ['proceed' => 'pdf'] :
                [
                    'modal' => $this->getRenderer()
                        ->render(
                            'layout/concern/modal',
                            [
                                'title' => 'Please chose additional questions answers',
                                'modal' => $this->getRenderer()->render(
                                    'layout/concern/letter/questions',
                                    [
                                        'questions' => $questions,
                                        'variants'  => $variants
                                    ]
                                )
                            ]
                        )
                ]
        );
    }

    /**
     * @return ViewModel
     */
    public function saveAction()
    {
        $content  = $this->getRequest()->getPost('content');
        $template = $this->getRequest()->getPost('template');
        $official = $this->getRequest()->getPost('official');

        if (null !== $template) {
            $template = $this->getEntityManager()->getRepository(Template::class)->find($template);
        }

        if (null === $template) {
            return new JsonModel(
                [
                    'error' => 'The template is not valid'
                ]
            );
        }

        if (null !== $official) {
            $official = $this->getEntityManager()->getRepository(Official::class)->find($official);
        }

        if (null === $official) {
            return new JsonModel(
                [
                    'error' => 'The official is not valid'
                ]
            );
        }

        $pdf = 'pdf/' . sha1(uniqid(uniqid())) . '.pdf';

        try {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($content);
            $dompdf->getCss()->load_css_file(BASE_PATH . DIRECTORY_SEPARATOR . 'css/stylesheet.css');
            $dompdf->render();

            $file = fopen(BASE_PATH . DIRECTORY_SEPARATOR . $pdf, 'w');

            if (!$file) {
                throw new \Exception();
            }

            fwrite($file, $dompdf->output());
            fclose($file);
        } catch (\Exception $exception) {
            return new JsonModel(
                [
                    'error' => 'Some error occurred while generating letter preview'
                ]
            );
        }

        $date = new \DateTime();

        $letter = new Letter();
        $letter->setContent($content);
        $letter->setCreated($date);
        $letter->setStatus(Letter::STATUS_OPEN);
        $letter->setPdf($pdf);
        $letter->setUser($this->getUser());
        $letter->setTemplate($template);
        $letter->setOfficial($official);
        $letter->setType($this->getRequest()->getPost('letter_type'));

        /** @var Options $options */
        $options = $this->getEvent()->getApplication()->getServiceManager()->get('options');

        $letterCount = $this->getEntityManager()
            ->getRepository(LetterCount::class)
            ->findOneBy(
                [
                    'week'  => $date->format('n'),
                    'year'  => $date->format('Y')
                ]
            );

        if (null === $letterCount) {
            $letterCount = new LetterCount();
            $letterCount->setCount(0);
            $letterCount->setYear($date->format('Y'));
            $letterCount->setWeek($date->format('W'));

            $this->getEntityManager()->persist($letterCount);
            $this->getEntityManager()->flush($letterCount);
        }

        $interval = $letterCount->getIntervalName();

        $type = $letter->getType() === 'US Mail' ? 'mail_' : 'fax_';

        $priceOption = $options->get($type . 'price_' . $interval);

        $additionalPrice = (float)($dompdf->getCanvas()->get_page_count() - 1) * (float)$options->get($type . 'price_additional_page');

        $price = (float)$priceOption + $additionalPrice;

        $letter->setPrice($price);

        $responsible = $options->get('responsible_' . $interval);
        $letter->setResponsibleEmail($responsible);

        $transaction = new Transaction();
        $transaction->setStatus(Transaction::STATUS_WAIT);
        $transaction->setPaymentId('');
        $transaction->setApproveUrl('');
        $this->getEntityManager()->persist($transaction);
        $letter->setTransaction($transaction);

        $this->getEntityManager()->persist($letter);
        $this->getEntityManager()->flush();

        return new JsonModel(
            [
                'html' => $this->getRenderer()
                    ->render(
                        'layout/concern/pdf-preview',
                        [
                            'letter' => $letter,
                            'price'  => [
                                'total'      => $price,
                                'letter'     => (float)$priceOption,
                                'additional' => $additionalPrice
                            ]
                        ]
                    )
            ]
        );
    }

    /**
     * @return array|JsonModel
     */
    public function payAction()
    {
        if (false === $this->getRequest()->isPost()) {
            return $this->notFoundAction();
        }

        /** @var Letter $letter */
        $letter = $this->getEntityManager()
            ->getRepository(Letter::class)
            ->findOneBy(
                [
                    'id'     => $this->getRequest()->getPost('letter'),
                    'status' => Letter::STATUS_OPEN,
                    'user'   => $this->getUser()
                ]
            );

        if (null === $letter) {
            return new JsonModel(
                [
                    'error' => 'Can not proceed to payment'
                ]
            );
        }

        try {

            /** @var Payment $payment */
            $payment = $this->getEvent()
                ->getApplication()
                ->getServiceManager()
                ->get('letterPayment')
                ->getPayment(
                    $letter->getTransaction(),
                    $letter->getPrice(),
                    $this->url()->fromRoute(
                        'home',
                        [],
                        ['force_canonical' => true]
                    )
                );

            $approveUrl = $payment->getApprovalLink();

            $transaction = $letter->getTransaction();
            $transaction->setPaymentId($payment->getId());
            $transaction->setApproveUrl($approveUrl);

            $this->getEntityManager()->persist($transaction);
            $this->getEntityManager()->persist($letter);
            $this->getEntityManager()->flush();
        } catch (PayPalConnectionException $payPalConnectionException) {
            return new JsonModel(
                [
                    'error' => 'Payment validation not success'
                ]
            );
        }

        return new JsonModel(
            [
                'success' => 'Payment is on wait',
                'url'     => $approveUrl
            ]
        );
    }

    /**
     * @return array|ViewModel
     */
    public function listAction()
    {
        if (null === $this->getUser()) {
            return $this->notFoundAction();
        }

        /** @var Letter[] $letters */
        $letters = $this->getEntityManager()
            ->getRepository(Letter::class)
            ->findBy(
                [
                    'user' => $this->getUser()
                ]
            );

        return new ViewModel(
            [
                'letters' => $letters
            ]
        );
    }

    /**
     * @return array|ViewModel
     */
    public function chooseAction()
    {
        /** @var Template $template */
        $template = $this->getEntityManager()->getRepository(Template::class)->find($this->params('template'));

        if (null === $template) {
            return $this->notFoundAction();
        }

        try {
            $api = $this->api()->getApi();

            if (null === $this->getUser()) {
                throw new \Exception('User can not be null for address information fetch');
            }

            $message = $api->request(
                Api::METHOD_REPRESENTATIVE_INFO,
                [
                    'address' => implode(' ', [
                        $this->getUser()->getSettings()->getAddressLine(),
                        $this->getUser()->getSettings()->getCity(),
                        $this->getUser()->getSettings()->getState(),
                        $this->getUser()->getSettings()->getZip(),
                    ])
                ]
            );

            $result = Json::decode($message->getBody()->getContents(), Json::TYPE_ARRAY);

            $officialsByAddress = Finder::findOfficialsByDivisions(array_keys($result['divisions']));
        } catch (\Exception $exception) {
            $officialsByAddress = [];
        }

        $data = [
            'templateId'         => $template->getId(),
            'divisionOfficials'  => [],
            'officeOfficials'    => [],
            'officials'          => [],
            'officialsByAddress' => $officialsByAddress
        ];

        /** @var TemplatePipeline[] $pipelines */
        $pipelines = $this->getEntityManager()
            ->getRepository(TemplatePipeline::class)
            ->findBy(
                [
                    'template' => $template
                ]
            );

        foreach ($pipelines as $pipeline) {
            if (null !== $pipeline->getDivision()) {

                /** @var Office $offices */
                $offices = $this->getEntityManager()->getRepository(Office::class)->findBy(
                    [
                        'division' => $pipeline->getDivision()
                    ]
                );

                $criteria = new Criteria();
                $criteria->where($criteria->expr()->in('office', $offices));

                $officials = $this->getEntityManager()->getRepository(Official::class)->matching($criteria);
                $data['divisionOfficials'][$pipeline->getDivision()->getName()] = $officials->toArray();

            } elseif (null !== $pipeline->getOffice()) {
                $officials = $this->getEntityManager()
                    ->getRepository(Official::class)
                    ->findBy(
                        [
                            'office' => $pipeline->getOffice()
                        ]
                    );
                $data['officeOfficials'][$pipeline->getOffice()->getName()] = $officials;
            } elseif (null !== $pipeline->getOfficial()) {
                $data['officials'][] = $pipeline->getOfficial();
            }
        }

        return new ViewModel($data);
    }

    /**
     * @return array|JsonModel
     */
    public function previewAction()
    {
        $id = $this->getRequest()->getPost('template');

        if (null === $id
            || null === ($template = $this->getEntityManager()->getRepository(Template::class)->find($id))
        ) {
            return $this->notFoundAction();
        }

        /** @var Template $template */

        return new JsonModel(
            [
                'modal' => $this->getRenderer()
                    ->render(
                        'layout/concern/modal',
                        [
                            'title' => $template->getName() . ' preview',
                            'modal' => $this->getRenderer()
                                ->render(
                                    'layout/concern/letter/preview',
                                    [
                                        'template' => $template
                                    ]
                                )
                        ]
                    )
            ]
        );
    }

}