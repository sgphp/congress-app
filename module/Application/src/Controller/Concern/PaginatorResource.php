<?php

namespace Application\Controller\Concern;

use Application\Module;
use Doctrine\Common\Collections\Criteria;
use DoctrineModule\Paginator\Adapter\Selectable;
use Zend\Paginator\Paginator;

/**
 * Class PaginatorResource
 * @package Application\Controller\Concern
 */
trait PaginatorResource
{

    /**
     * @var Criteria
     */
    private $criteria;

    /**
     * PaginatorResource constructor.
     */
    public function __construct()
    {
        $this->criteria = new Criteria();
    }

    /**
     * @param string $entity
     * @param $currentPage
     * @param int $itemsPerPage
     * @return Paginator
     */
    protected function paginator(string $entity, $currentPage, $itemsPerPage = 20)
    {
        $adapter = new Selectable(
            Module::entityManager()->getRepository($entity),
            $this->criteria
        );

        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($currentPage);
        $paginator->setItemCountPerPage($itemsPerPage);

        $this->criteria = new Criteria();

        return $paginator;
    }

}

