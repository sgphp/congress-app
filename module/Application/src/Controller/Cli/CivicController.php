<?php

namespace Application\Controller\Cli;

use Application\Service\Civic\Api;

use Application\Service\Civic\Thread\StateParser;
use GuzzleHttp\Promise\EachPromise;
use Psr\Http\Message\ResponseInterface;
use Zend\Http\Request;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class CivicController
 * @package Admin\Controller\Cli
 * @method \Application\Controller\Plugin\Api api()
 */
class CivicController extends AbstractActionController
{

    /**
     * Guzzle parallel request grab action
     */
    public function grabAction()
    {
        $config = $this->getEvent()->getApplication()->getServiceManager()->get('Config');
        $api = $this->api()->getApi();

        $stateParser = new StateParser();

        $promisesFunction = function () use ($api, $config, $stateParser) {
            foreach ($config['states'] as $code => $state) {
                /** @var Api $api */
                yield $api->getClient()->requestAsync(
                    Request::METHOD_GET,
                    $api->url(
                        Api::METHOD_REPRESENTATIVE_INFO . '/' . urlencode('ocd-division/country:us/state:' . strtolower($code)),
                        [
                            'recursive' => 'true',
                            'alt' => 'json'
                        ]
                    )
                )->then(
                    function (ResponseInterface $response) use ($stateParser) {
                        /** @var StateParser $stateParser */
                        $results = Json::decode($response->getBody(), Json::TYPE_ARRAY);

                        $stateParser->pushResults($results);
                        $stateParser->run();
                    },
                    function () {
                        echo 'Some error happens' . PHP_EOL;
                    }
                );
            }
        };

        $promises = $promisesFunction();

        (new EachPromise($promises))->promise()->wait();
    }

}