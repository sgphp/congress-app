<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractController
{

    /**
     * @return ViewModel
     */
    public function aboutAction()
    {
        return new ViewModel();
    }

    /**
     * Information action
     * @return ViewModel
     */
    public function informationAction()
    {
        return new ViewModel;
    }

    /**
     * @return ViewModel
     */
    public function termsAction()
    {
        return new ViewModel();
    }

    /**
     * @return ViewModel
     */
    public function faqAction()
    {
        return new ViewModel();
    }

}
