<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

/**
 * Class FramesController
 * @package Application\Controller
 */
class FramesController extends AbstractController
{

    /**
     * @return ViewModel
     */
    public function headerAction() {
        return (new ViewModel(
            [
                'styles' => true,
                'user'   => $this->getUser()
            ]
        ))
            ->setTemplate('layout/concern/header')
            ->setTerminal(true);
    }

    /**
     * @return ViewModel
     */
    public function footerAction() {
        return (new ViewModel(
            [
                'styles' => true,
            ]
        ))
            ->setTemplate('layout/concern/footer')
            ->setTerminal(true);
    }

}