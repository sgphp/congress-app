<?php

namespace Application\Controller;

use Application\Controller\Plugin\Api;
use Application\Model\User;
use Application\Module;
use Doctrine\ORM\EntityManager;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Service\ViewPhpRendererFactory;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class AbstractController
 * @package Application\Controller
 * @method Api api()
 */
abstract class AbstractController extends AbstractActionController
{

    /**
     * @inheritdoc
     */
    protected function init()
    {
    }

    /**
     * @throws \Exception
     */
    protected function initAcl()
    {
        if ( false === $this->getEvent()
                ->getViewModel()
                ->getVariable('acl')
                ->isAllowed(
                    null !== $this->getUser() ? $this->getUser()->getRole() : 'guest',
                    $this->getEvent()->getRouteMatch()->getMatchedRouteName()
                )
        ) {
            throw new \Exception( 'Can not load page', -1000 );
        }
    }

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $e->getViewModel()->setVariable('user', $this->getUser());
        $this->init();

        try {
            $this->initAcl();
        } catch (\Exception $exception) {
            if ($exception->getCode() === -1000) {
                return $this->notFoundAction();
            }
        }

        $result = parent::onDispatch($e);

        if (true === $this->getRequest()->isXmlHttpRequest()
            && get_class($result) === ViewModel::class
        ) {

            $controllerParts = explode('\\', $this->params('controller'));
            $controllerName = end($controllerParts);
            $moduleName = strtolower($controllerParts[0]);

            $controllerPath = preg_replace('/(\w+)Controller/', '$1', $controllerName);
            $controllerPath = strtolower($controllerPath);
            $template = implode('/', [$moduleName, $controllerPath, $this->params('action')]);
            $result->setTemplate($template);
            $result = new JsonModel(
                [
                    'html' => $this->getRenderer()->render($result)
                ]
            );

            $e->setResult($result);
        }

        return $result;
    }

    /**
     * @return EntityManager mixed
     */
    public function getEntityManager()
    {
        return Module::entityManager();
    }

    /**
     * @return AuthenticationService
     */
    public function getAuth()
    {
        return $this->getEvent()
            ->getApplication()
            ->getServiceManager()
            ->get('auth');
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->getAuth()->getStorage()->read();
    }

    /**
     * @return PhpRenderer
     */
    public function getRenderer()
    {
        $factory = new ViewPhpRendererFactory();
        $renderer = $factory($this->getEvent()->getApplication()->getServiceManager(), 'renderer#');
        return $renderer;
    }

    /**
     * @param bool $role
     * @return \Zend\Http\Response
     */
    public function restrictNonLoggedIn($role = false)
    {

        $user = $this->getUser();

        if (null === $user || (false !== $role && true === in_array($user->getRole(), (array)($role)))) {
            return $this->redirect()->toRoute('user', ['action' => 'login']);
        }
    }

}