<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Application\Service\Civic\Api as CivicApi;

/**
 * Class Api
 * @package Application\Controller
 * @method AbstractActionController getController()
 */
class Api extends AbstractPlugin
{

    /**
     * @return CivicApi
     */
    public function getApi()
    {
        $config = $this->getController()->getEvent()->getApplication()->getServiceManager()->get('Config');
        $api = new CivicApi($config['civic_api']['key']);

        return $api;
    }

}