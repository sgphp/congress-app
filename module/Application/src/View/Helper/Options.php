<?php

namespace Application\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

/**
 * Class Options
 * @package Application\View
 */
class Options extends AbstractHelper
{

    /**
     * @var ServiceManager
     */
    private $serviceManager;

    /**
     * Options constructor.
     * @param ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @param $name
     * @return string|null
     */
    public function get($name)
    {
        /** @var \Application\Service\Options $serviceOption */
        $serviceOption = $this->serviceManager->get('options');
        return $serviceOption->get($name);
    }

}