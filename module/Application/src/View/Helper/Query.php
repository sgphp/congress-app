<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class Query
 * @package Application\View
 */
class Query extends AbstractHelper
{

    /**
     * @var array
     */
    protected $query;

    /**
     * Query constructor.
     * @param $query
     */
    public function __construct(array $query) {
        $this->query = $query;
    }

    /**
     * @param array $append
     * @return array
     */
    public function merge(array $append)
    {
        return array_replace_recursive($this->query, $append);
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->query;
    }
}