<?php

namespace Application;

use Application\Mail\Sender;
use Application\Model\User;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;

/**
 * Class Module
 * @package Application
 */
class Module
{

    /**
     * Version of alpha
     */
    const VERSION = '0.0.1-alpha';

    /**
     * @var null|EntityManager
     */
    protected static $entityManager;

    /**
     * @var null|Sender
     */
    protected static $mailSender;

    /**
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return include __DIR__ . '/../config/service.config.php';
    }

    /**
     * @return array
     */
    public function getViewHelperConfig() {
        return include __DIR__ . '/../config/view.config.php';
    }

    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        $this->setEntityManager($event);
        $this->setMailSender($event);
        $event->getViewModel()->getVariables();

        $event->getViewModel()->setVariable('acl', $this->getAcl($event));
    }

    /**
     * @return EntityManager|null
     */
    public static function entityManager()
    {
        return static::$entityManager;
    }

    /**
     * @return Sender|null
     */
    public static function getMailSender()
    {
        return static::$mailSender;
    }

    /**
     * @param MvcEvent $event
     */
    public function setEntityManager(MvcEvent $event)
    {
        static::$entityManager = $event
            ->getApplication()
            ->getServiceManager()
            ->get('Doctrine\ORM\EntityManager');
    }

    /**
     * @param MvcEvent $event
     */
    public function setMailSender(MvcEvent $event)
    {
        static::$mailSender = $event
            ->getApplication()
            ->getServiceManager()
            ->get('mail');
    }

    /**
     * @param MvcEvent $event
     * @return Acl
     */
    public function getAcl(MvcEvent $event)
    {
        $acl = new Acl();

        $acl->addRole(User::ROLE_SUSPENDED)
            ->addRole('guest', User::ROLE_SUSPENDED)
            ->addRole(User::ROLE_USER, 'guest');

        foreach (array_keys(include __DIR__ . '/../config/routes.config.php') as $resource) {
            $acl->addResource($resource);
        }

        $acl->allow(
            'guest',
            [
                'home',
                'index',
                'user',
                'civic_api',
                'service',
                'recovery-password',
                'recovery-password-cancel',
                'letter',
                'frames',
                'category-letter',
                'tag-letter',
                'compose-letter',
                'compose-letter-template',
                'letter-ipn',
                'choose-letter-template-legislator'

            ]
        );

        $acl->allow(
            User::ROLE_USER,
            [
                'show-user',
            ]
        );

        return $acl;
    }

    /**
     * @return array
     */
    protected function getRouteNames()
    {
        $routes = include __DIR__ . '/../config/routes.config.php';
        return array_keys($routes);
    }

}
