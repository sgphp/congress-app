// Main application js file

/**
 * Submit form with ajax
 */
jQuery(document).on('submit', 'form.async', function (event) {
    ajaxFormSubmit(event);
    return false;
});

var ajaxFormSubmit = function (event) {
    event.defaultPrevented = true;

    var form  = jQuery(event.target);

    var formData       = new FormData;
    var serializedForm = form.serializeArray();

    for (var i in serializedForm) {
        var input = serializedForm[i];
        formData.append(input.name, input.value);
    }

    [].slice.call(form.find('[type="file"]')).forEach(function (fileInput) {

        var files = fileInput.files;

        for (var i in files) {
            var file = files[i];

            if (file instanceof File && file.type.match('.*')) {
                formData.append($(fileInput).attr('name'), file);
            }
        }
    });

    var callback = function () {};

    if (jQuery(event.target).data('controller') !== undefined && jQuery(event.target).data('action') !== undefined) {
        var dispatcherResource = getDispatcherResource(
            jQuery(event.target).data('controller'),
            jQuery(event.target).data('action'),
            event
        );

        callback = dispatcherResource.action;
    } else {
        callback = function (response) {
            Validate.showErrorsMessages(response.errors);
            Validate.redirect(response.redirect);
        }
    }

    jQuery.ajax(
        {
            url: form.attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            method: 'post',
            success: callback
        }
    );
};

/**
 * @type {{showErrorsMassages: Validate.showErrorsMessages, redirect: Validate.redirect}}
 */
var Validate = {

    /**
     * show errors block under form fields
     * @param errors
     */
    showErrorsMessages: function(errors) {
        jQuery('.errors-block').remove();

        if (errors !== undefined) {
            for (var field in errors) {
                if (errors[field] !== undefined){

                    $.each(errors[field], function( index, massage ) {

                        if (jQuery("input[name='" + field + "']").closest('.form-group').length === 0){
                            jQuery("input[name='" + field + "']").closest('.input-group').after('<div class="label errors-block label-danger" style="padding-top: -15px;">' + massage + '</div>');
                        }

                        jQuery("input[name='" + field + "']").closest('.form-group').append('<div class="label errors-block label-danger">' + massage + '</div>');
                        jQuery("select[name='" + field + "']").after('<span class="label errors-block label-danger">' + massage + '</span>');
                    });
                }
            }
        }
    },

    redirect: function (url) {
        if (url !== undefined) {
            window.location.assign(url);
        }
    }
};

var cloneObject = function(object) {
    var newObj = (object instanceof Array) ? [] : {};
    for (var i in object) {
        if (i == 'cloneAll') continue;
        if (object[i] && typeof object[i] == "object") {
            newObj[i] = object[i].cloneAll();
        } else newObj[i] = object[i]
    } return newObj;
};

jQuery(document).on('click', '.prepare-data', function (event) {
    event.defaultPrevented = true;

    var element = jQuery(event.target);

    jQuery(':not(#letter-' + element.attr('id') + ')[id^="letter-"] a').hide();

    jQuery(document).find(element.data('loader')).removeClass('hidden');
});

jQuery(document).on('change', "#font", function() {
    var signatureInput = jQuery('#signature');

    signatureInput
        .css("font-size", $('#font').find('option:selected').data('size'))
        .css("font-family", $(this).val());

});

jQuery(document).on('click', '.group-activation > button', function (event) {
    jQuery(event.target).parent().find('button.active').removeClass('active');
    jQuery(event.target).addClass('active');
});