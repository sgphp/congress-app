jQuery(document).on('click', '#add_variant', function () {

    var variantsPlace = jQuery('#variants_place');

    if (variantsPlace.find('input.variant[type="text"][value=""]').length === 0) {
        var inputVariant = variantsPlace.find('input.variant[type="hidden"]').clone();
        var inputCompiledValue = variantsPlace.find('input.compiled_value[type="hidden"]').clone();
        inputVariant.attr('type', 'text');
        inputCompiledValue.attr('type', 'text');
        variantsPlace.append(inputVariant);
        variantsPlace.append(inputCompiledValue);
    }

    return false;
});

jQuery(document).on('click', '#add_question', function () {

    var questionsPlace = jQuery('#questions_place');
    var variantsPlace = jQuery('#variants_place');
    var inputsPlace = jQuery('#real_question_place');
    var question = jQuery('#question');

    var variants = [].slice.call(variantsPlace.find('input.variant[type="text"]:not([value=""])')).map(
        function (variant) {
            return jQuery(variant).val();
        }
    );

    var variantsString = '';

    variants.forEach(function (variant) {
        variantsString += '<li>' + variant + '</li>'
    });

    var questionBlock = questionsPlace
        .find('.question-block.hidden')
        .clone();

    questionsPlace.append(
        questionBlock
            .removeClass('hidden')
            .find('h5')
            .html(question.val())
            .parent()
            .find('ul')
            .append(variantsString)
            .parent()
    );

    var inputsPrepared = inputsPlace.find('.inputs-prepared');
    var inputsClone = inputsPrepared.clone();

    inputsClone.removeClass('inputs-prepared').addClass('inputs-done');
    inputsPlace.append(inputsClone);

    variants.forEach(function (variant) {
        inputsClone.append(
            inputsPrepared.find('input[name^="variant"]')
                .clone()
                .removeClass('prepared')
                .val(variant)
                .prop('disabled', false)

        );
    });

    var compiledValues = [].slice.call(variantsPlace.find('input.compiled_value[type="text"]:not([value=""])')).map(
        function (input) {
            return jQuery(input).val();
        }
    );

    compiledValues.forEach(function (compiledValue) {
        inputsClone.append(
            inputsPrepared.find('input[name^="compiled_value"]')
                .clone()
                .removeClass('prepared')
                .val(compiledValue)
                .prop('disabled', false)

        );
    });

    var beginning = jQuery('input#beginning').val();

    inputsClone.find('input[name^="question_beginning"]')
        .removeClass('prepared')
        .val(beginning)
        .prop('disabled', false);

    inputsClone.find('input[name^="multiples"]')
        .removeClass('prepared')
        .val(jQuery('input#multiple_choose').prop('checked') ? 'on' : 'off')
        .prop('disabled', false);

    inputsClone.find('input#main[name^="question"]').val(question.val()).prop('disabled', false);
    inputsClone.attr('id', inputsClone.attr('id').replace(/#/g, inputsPlace.find('.inputs-done').length));
    questionBlock.attr('id', questionBlock.attr('id').replace(/#/g, inputsPlace.find('.inputs-done').length));
    questionsPlace.find('a[id^="remove_question"]').attr('id', questionsPlace.find('a[id^="remove_question"]').attr('id').replace(/#/g, inputsPlace.find('.inputs-done').length));
    inputsClone.html(inputsClone.html().replace(/#/g, inputsPlace.find('.inputs-done').length));

    question.val('');
    variantsPlace.find('input[type="text"]').remove();

    return false;
});

jQuery(document).on('change', '#multiple_choose', function (event) {

    var checkbox = jQuery(event.target);
    var beginning = jQuery('#beginning');

    if (checkbox.prop('checked')) {
        beginning.removeClass('hidden');
    } else {
        beginning.addClass('hidden');
        beginning.val('');
    }

});

jQuery(document).on('click', 'a[id^="remove_question"]', function (event) {


    var id = jQuery(event.target).attr('id').replace(/remove_question_(\d+)/, '$1');

    jQuery('#question_block_' + id).remove();
    jQuery('#question_inputs_' + id).remove();

    return false;
});