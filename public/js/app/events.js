/**
 * Event listen for controller call
 */
jQuery(document).on('click', '.call-action', function (event) {
    event.defaultPrevented = true;
    event.stopPropagation();

    var element = jQuery(event.target);

    dispatch(element.data('controller'), element.data('action'), event);

    return false;
});

jQuery(document).on('click', 'a.as', function (event) {
    event.defaultPrevented = true;

    var mainContent = jQuery('#main_content');

    var beforeCalls = new callBefore(event);

    if (jQuery(event.target).data('before') && beforeCalls.hasOwnProperty(jQuery(event.target).data('before'))) {
        beforeCalls[jQuery(event.target).data('before')]();
    }

    callWeb(event.target.href, [], function (result) {

        var afterCalls = new callAfter(result);

        if (jQuery(event.target).data('after') && afterCalls.hasOwnProperty(jQuery(event.target).data('after'))) {
            afterCalls[jQuery(event.target).data('after')]();

            if (result.hasOwnProperty('html')) {
                jQuery('#main_content').html(result.html);
            }
        } else {
            mainContent.html(result.html);
        }

        window.history.pushState('', '', event.target.href);
    }, WEB.METHOD_GET);

    return false;
});

jQuery(document).ready(function () {

    var WYSIWYG = jQuery('textarea#wys');

    if (WYSIWYG.length) {
        callWeb(
            baseUrl + 'admin/interactive/variables',
            {},
            function (response) {
                WYSIWYG.summernote(
                    {
                        height: 550,
                        hint: {
                            mentions: response.variables,
                            match: /\B@@(\w*)$/,
                            search: function (keyword, callback) {
                                callback($.grep(this.mentions, function (item) {
                                    return item.indexOf(keyword) === 0;
                                }));
                            },
                            content: function (item) {
                                return '<<<' + item + '>>>';
                            }
                        }
                    }
                );
            }
        );
    }
});

jQuery(document).on('click', '#add-pipeline', function (event) {
    event.defaultPrevented = true;

    var serviceModal = '#admin-modal';

    var by = jQuery('input[type="radio"][name="type"]:checked').attr('id').replace(/pipeline_(.+)/, '$1');

    var name = 'general';
    var val  = 'true';

    var select = jQuery(serviceModal).find('select#search_pipeline');

    switch (by) {
        case 'division':
            name = 'divisions[]';
            val = select.find('option:selected').val();
            break;
        case 'office':
            name = 'offices[]';
            val = select.find('option:selected').val();
            break;
        case 'official':
            name = 'officials[]';
            val = select.find('option:selected').val();
            break;
    }

    // Append hidden input
    jQuery(
        '<input/>', {
            type : 'hidden',
            name : name,
            value: val
        }
    ).appendTo('form[action*="store-template"]');

    // Append hidden break-line block
    jQuery(
        '<br>'
    ).appendTo('.pipelines-area');

    // Append label
    jQuery(
        '<div/>', {
            class: 'label label-info',
            text :  by + ': ' + select.find('option:selected').html()
        }
    ).appendTo('.pipelines-area');

    return false;
});

jQuery(document).ready(function () {

    jQuery('.summernote-air').summernote(
        {
            airMode: true
        }
    );

    jQuery('#main_content').bind("DOMSubtreeModified",function(){
        var summernotes;
        if (!jQuery('.note-editor').length && (summernotes = jQuery('.summernote-air')).length) {
            summernotes.summernote(
                {
                    airMode: true
                }
            );
        }
    });

    jQuery('#tags').select2({
        tags: true,
        ajax: {
            url: baseUrl + 'admin/interactive/search-tags',
            processResults: function (data) {
                return {
                    results: data.tags.map(
                        function (tag) {
                            return {
                                id: tag,
                                text: tag
                            };
                        }
                    )
                };
            }
        }
    });

    jQuery('#category').select2({
        ajax: {
            url: baseUrl + 'admin/interactive/search-category',
            processResults: function (data) {
                return {results: data['categories']};
            }
        }
    });

    jQuery('#choose-legislator').select2(
        {
            theme: 'classic',
            ajax: {
                url: baseUrl + 'rest/service/by-officials-render',
                processResults: function (data) {
                    return {
                        results: data.values
                    }
                }
            },
            escapeMarkup: function (markup) { return markup; }
        }
    );
});

jQuery(document).on('change', 'input[type="radio"][name="type"]', function () {

    var serviceModal = '#admin-modal';

    var ajaxUrl = jQuery(serviceModal).find('input[type="radio"][name="type"]:checked').val();
    var select = jQuery(serviceModal).find('select#search_pipeline');

    select.select2(
        {
            ajax: {
                url: ajaxUrl,
                processResults: function (data) {
                    return {
                        results: data.values
                    }
                }
            }
        }
    );
});

/**
 * When choosing legislator options may have different height
 * This event handling adding style attribute to parent .select2 span
 */
jQuery(document).on('select2:select', '#choose-legislator', function () {
    var selection = jQuery('.select2-selection');
    var height = selection.find('span.select2-selection__rendered').css('height').replace(
        /(\d+).*/,
        /**
         * ___ variable has no name, because no usage. Skipped for more Informativity
         */
        function (___, px) {
            return (parseInt(px) + 20) + 'px';
        }
    );
    selection.attr('style', 'height: ' + height + ';padding-top: 10px;');
});

function proceedToComposeLetter(template, legislator) {
    callWeb(
        baseUrl + 'letter/compose-template/' + template + '/' + legislator,
        [],
        function (response) {
            var afterCall = new callAfter(response);
            afterCall.checkForIdentity();

            if (response.hasOwnProperty('html')) {
                jQuery('#main_content').html(response.html);
            }
        }
    );
}

jQuery(document).on('click', '#choose_legislator', function () {
    proceedToComposeLetter(
        jQuery('input[name="template"]').val(),
        jQuery('#choose-legislator').find('option:selected').val()
    );
});

jQuery(document).on('click', '.select-legislator', function (event) {
    proceedToComposeLetter(
        jQuery('input[name="template"]').val(),
        jQuery(event.target).data('legislator')
    );
});

jQuery(document).on('click', '#ok_done_questions', function () {

    [].slice.call(jQuery('.questions-inner')).forEach(function (block) {

        var answers = [];

        block = jQuery(block);

        answers.push(
            [].slice.call(block.find('input[type="checkbox"]:checked.question-no-free')).map(
                function (option) {
                    return jQuery(option).val();
                }
            ).join(', ')
        );

        answers = answers.concat(
            [].slice.call(block.find('input[type="radio"]:checked.question-no-free')).map(
                function (option) {
                    return jQuery(option).val();
                }
            )
        );

        answers = answers.concat(
            [].slice.call(block.find('input.question-free')).map(
                function (input) {
                    var answer = jQuery(input).val();

                    if (answer.match(/\.$/) === null) {
                        answer += '.';
                    }

                    return answer;
                }
            )
        );

        if (answers !== []) {
            putAnswersPlace(answers, block.find('.question_beginning').val());
        }
    });

    var airBlock = jQuery('.summernote-air');
    if (airBlock.length === 1) {
        airBlock.summernote(
            'code',
            airBlock
                .summernote('code')
                .replace(/&lt;&lt;&lt;Answers place&gt;&gt;&gt;/, '')
        );
    }

    jQuery('#service-modal').modal('hide');

    return false;
});

jQuery(document).on('click', '.skip-questions', function () {
    var airBlock = jQuery('.summernote-air');
    if (airBlock.length === 1) {
        airBlock.summernote(
            'code',
            airBlock
                .summernote('code')
                .replace(/&lt;&lt;&lt;Answers place&gt;&gt;&gt;/, '')
        );

    }
    jQuery('#service-modal').modal('hide');
});

function putAnswersPlace(answers, beginning) {
    var airBlock = jQuery('.summernote-air');

    if (undefined === beginning) {
        beginning = '';
    }

    if (airBlock.length === 1) {
        airBlock.summernote(
            'code',
            airBlock
                .summernote('code')
                .replace(
                    /(&lt;&lt;&lt;Answers place&gt;&gt;&gt;)/,
                    beginning + ' ' + answers.join(' ') + '$1<br>'
                )
        );
    }
}

jQuery(document).on('click', '[data-dismiss="alert"]', function (event) {
    jQuery(event.target).parent().remove();
});

function setImageData(data) {
    jQuery('#preview-signature').find('img').attr('src', data);
    jQuery('#signature_base64').val(data);
}

jQuery(document).on('change', 'input#selector-image-upload[type="file"]', function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var data = e.target.result;
            setImageData(data);
        };

        reader.readAsDataURL(this.files[0]);
    }
});

jQuery(document).on('click', '#remove_signature_image', function (event) {
    event.defaultPrevented = true;

    setImageData('');
    event.target.remove();

    return false;
});

jQuery(document).on('click', '#apply_sketch', function () {

    var canvas = document.getElementById('sketchpad');
    setImageData(canvas.toDataURL());

    return false;
});

$.fn.modal.Constructor.prototype.enforceFocus = function() {};