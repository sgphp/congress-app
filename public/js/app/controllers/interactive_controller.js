/**
 * @constructor
 */
var interactiveController = function () {

    this.next = {
        url: baseUrl + 'admin/interactive/divisions',
        params: []
    };

    this.pipeline = {
        url: baseUrl + 'admin/interactive/add-pipeline',
        params: []
    };

    this.nextAction = function (response) {
        if (response.hasOwnProperty('html')) {
            jQuery('#divisions-place').html(response.html);
        }
    };

    this.officesAction = function (response) {
        if (response.hasOwnProperty('html')) {
            jQuery('#offices-place').html(response.html);
        }
    };

    this.officialsAction = function (response) {
        if (response.hasOwnProperty('html')) {
            jQuery('#officials-place').html(response.html);
        }
    };

    this.pipelineAction = function (response) {
        if (response.hasOwnProperty('modal')) {

            var serviceModal = '#admin-modal';

            if (jQuery(serviceModal).length !== 0) {
                jQuery(serviceModal).modal('hide');
                jQuery(serviceModal).remove();
            }

            jQuery('body').eq(0).append(response.modal);
            jQuery(serviceModal).modal();

        }
    };

};