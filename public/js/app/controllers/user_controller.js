/**
 * @constructor
 */
var userController = function () {

    var serviceModal = '#service-modal';

    this.login = {
        url: baseUrl + 'user',
        params: {
            email: jQuery(serviceModal).find('input[name="email"]').val(),
            password: jQuery(serviceModal).find('input[name="password"]').val()
        },
        method: WEB.METHOD_POST
    };

    this.register = {
        url: baseUrl + 'user/register',
        params: {
            email: jQuery(serviceModal).find('input[name="email"]').val(),
            password: jQuery(serviceModal).find('input[name="password"]').val()
        },
        method: WEB.METHOD_POST
    };

    this.registerModal = {
        url: baseUrl + 'user/register',
        params: {
            modal: 'modal'
        },
        method: WEB.METHOD_GET
    };

    this.loginAction = function (response) {
        if (response.hasOwnProperty('errors')) {
            Validate.showErrorsMessages(response.errors);
        }

        if (response.hasOwnProperty('redirect')) {

            replayLayout();

            window.location.reload();
            jQuery(serviceModal).modal('toggle');
        }
    };

    this.registerAction = function (response) {
        if (response.hasOwnProperty('errors')) {
            Validate.showErrorsMessages(response.errors);
        }

        if (response.hasOwnProperty('redirect')) {
            dispatch('civic', 'representativeInfo');
            jQuery(serviceModal).modal('toggle');
            replayLayout();
        }
    };

    this.registerModalAction = function (response) {
        if (response.hasOwnProperty('modal')) {

            var serviceModal = '#service-modal';

            if (jQuery(serviceModal).length !== 0) {
                jQuery(serviceModal).modal('hide');
                jQuery(serviceModal).remove();
            }

            jQuery('body').eq(0).append(response.modal);
            jQuery(serviceModal).modal();

            jQuery(serviceModal).find('form').data('controller', 'user').data('action', 'register');
        }
    }
};