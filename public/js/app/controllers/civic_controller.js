/**
 * @constructor
 * @magic
 */
var civicController = function () {
    'use strict';

    var streetValue = jQuery('input[name="street_address"]').val();
    var cityValue   = jQuery('input[name="city"]').val();
    var zipValue    = jQuery('input[name="zip"]').val();
    var stateValue  = jQuery('select[name="state"] > option:selected').val();

    this.representativeInfo = {
        url: baseUrl + 'rest/civic/representative-info',
        params: {
            address: [streetValue, cityValue, zipValue, stateValue].join(' '),
            street: streetValue,
            city:   cityValue,
            zip:    zipValue,
            state:  stateValue
        },
        method: WEB.METHOD_POST
    };

    this.representativeInfoAction = function (result) {
        if (result.hasOwnProperty('html')) {
            jQuery('#main_content').html(result.html);
        }
    };
};