/**
 * @param event
 */
var letterController = function (event) {

    var templateElement = jQuery('input[name="template"]');

    this.questions = {
        url: baseUrl + 'letter/questions',
        params: {
            template: templateElement.val()
        }
    };

    this.save = {
        url: baseUrl + 'letter/save',
        params: {
            content: jQuery('.summernote-air').eq(0).summernote('code'),
            template: templateElement.val(),
            official: jQuery('input[name="official"]').val(),
            letter_type: jQuery('.group-activation').find('button.active').text()
        }
    };

    this.preview = {
        url: baseUrl + 'letter/preview',
        params: {
            template: event ? jQuery(event.target).data('template') : null
        }
    };

    this.questionsAction = function (response) {

        if (response.hasOwnProperty('modal')) {
            var serviceModal = '#service-modal';

            if (jQuery(serviceModal).length !== 0) {
                jQuery(serviceModal).remove();
            }

            jQuery('body').eq(0).append(response.modal);
            jQuery(serviceModal).modal();

            jQuery(serviceModal).find('form').data('controller', 'letter').data('action', 'preview');
        }
    };

    this.saveAction = function (response) {
        if (response.hasOwnProperty('html')) {
            jQuery('#main_content').html(response.html);
        }

        var serviceModal = '#service-modal';

        if (jQuery(serviceModal).length) {
            jQuery(serviceModal).modal('hide');
        }
    };

    this.payAction = function (response) {
        if (response.hasOwnProperty('url')) {
            window.location.assign(response.url);
        }
    };

    this.previewAction = function (response) {
        if (response.hasOwnProperty('modal')) {
            var serviceModal = '#service-modal';

            if (jQuery(serviceModal).length !== 0) {
                jQuery(serviceModal).remove();
            }

            jQuery('body').eq(0).append(response.modal);
            jQuery(serviceModal).modal();

            jQuery(serviceModal).find('form').data('controller', 'letter').data('action', 'preview');
        }
    }
};