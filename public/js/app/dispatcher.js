/**
 * @type {{object}}
 */
var WEB = {
    METHOD_POST:   'POST',
    METHOD_GET:    'GET',
    METHOD_PUT:    'PUT',
    METHOD_DELETE: 'DELETE'
};

var DispatcherResource = {
    controller: null,
    action: null,
    action_resource: '',
    valid: false
};

/**
 * @param controller
 * @param action
 * @returns {DispatcherResource}
 */
var getDispatcherResource = function (controller, action, event) {
    var dispatcherResource = cloneObject(DispatcherResource);

    var controllerRealName = controller + 'Controller';

    var actionRealName = action + 'Action';

    dispatcherResource.action_resource = action;

    var realController = eval(controllerRealName);
    realController = new realController(event);

    if (typeof realController === 'object') {
        dispatcherResource.controller = realController;

        if (typeof realController[actionRealName] === 'function') {
            dispatcherResource.action = realController[actionRealName];
            dispatcherResource.valid = true;
        }
    }

    if (dispatcherResource.valid === false) {
        // Todo make 404 page view
    }

    return dispatcherResource;
};


/**
 * @param controller
 * @param action
 */
var dispatch = function (controller, action, event) {

    var dispatcherResource = getDispatcherResource(controller, action, event);

    if (dispatcherResource.valid) {
        var resources = dispatcherResource.controller[dispatcherResource.action_resource];

        callWeb(
            resources.url,
            resources.params,
            dispatcherResource.action,
            resources.method
        );
    }
};

var replayLayout = function () {
    var callback = function (response) {
        for (var id in response) {
            var html = response[id];
            jQuery('#' + id).html(html);
        }
    };

    callWeb(
        baseUrl + 'rest/service/header-menu',
        [],
        callback
    );
};

/**
 * @param url
 * @param params
 * @param callback
 * @param method
 */
var callWeb = function (url, params, callback, method) {
    if (callback === undefined) {
        callback = function (result) { console.log(result) }
    }

    if (method === undefined) {
        method = WEB.METHOD_POST;
    }

    if (params === undefined) {
        params = [];
    }

    jQuery.ajax(
        {
            url: url,
            method: method,
            data: params,
            success: callback,
            error: function () {
                showAlert('Some error happens while calling')
            }
        }
    )
};

var callBefore = function (event) {

};

var callAfter = function (response) {
    this.checkForIdentity = function () {
        if (response.hasOwnProperty('modal')) {
            var serviceModal = '#service-modal';

            if (jQuery(serviceModal).length !== 0) {
                jQuery(serviceModal).remove();
            }

            jQuery('body').eq(0).append(response.modal);
            jQuery(serviceModal).modal();

            jQuery(serviceModal).find('form').data('controller', 'user').data('action', 'login');
        }
    }
};

/**
 * Event to handle errors in 200 OK Response
 */
jQuery(document).ajaxComplete(function(event, xhr) {
    var data = JSON.parse(xhr.responseText);

    if (data.hasOwnProperty('errors')) {
        var text = '';

        for (var input in data.errors) {
            text += Object.values(data.errors[input]).map(function(error) {
                return jQuery(
                    '<li/>',
                    {
                        html: jQuery(
                            '<b/>',
                            {
                                text: input
                            }
                        ).get(0).outerHTML + ': ' + error
                    }
                ).get(0).outerHTML
            });
        }

        text = '<ul>' + text + '</ul>';

        showAlert(text);
    } else if (data.hasOwnProperty('error')) {
        showAlert(data.error);
    }

});

/**
 * Show errors
 * @param html
 */
function showAlert(html) {
    var alertBlock = jQuery(
        '<div/>',
        {
            class: 'alert alert-fixed alert-danger fade',
            html:
                jQuery(
                    '<button/>',
                    {
                        type: 'button',
                        class: 'close',
                        'data-dismiss': 'alert',
                        'aria-label': 'Close',
                        html: jQuery(
                            '<span/>',
                            {
                                'aria-hidden': 'true',
                                html: '&times;'
                            }
                        ).get(0).outerHTML
                    }
                ).get(0).outerHTML + html + jQuery()
        }
    ).addClass("in");

    alertBlock.appendTo(jQuery(document.body));

    setTimeout(
        function () {
            alertBlock.fadeOut(750);
        },
        5000
    );
}
