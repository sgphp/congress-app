<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'congress-blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q<i SFw;AWad^UfuZVD@7)/hGelHgfD&RdmpJls.7RZ<K.>-;7Q0uLqgFo,X?frA');
define('SECURE_AUTH_KEY',  '%0k Jts-C2iRXw][S9-)A<2<>Fun=nUb63E;]A(_f55%$7@8k+[{lUGF?nClY-Ep');
define('LOGGED_IN_KEY',    '%[RVb_j8(#eeoE>RYS%(x7mg>gj@?F]ZaS,6*?S}vxRD/^}v`wK7o.h$m8Sn,s4/');
define('NONCE_KEY',        '%J>Q6|*gK@e4|->h2D9RvQW-k8rU1GRYTq>7P VD6xXM_j49-P5^ D2s/pb>6&AZ');
define('AUTH_SALT',        '1{JZ#HD%,C/}Ch7Za/K10u4/pXm<vJR9AfaeBS%$-tA &vXEg?R)2i<@`gjZ,[>&');
define('SECURE_AUTH_SALT', '#Cdt},%bpbf[hkZZ7sc{<Ltx|~u+G!%~[1ngAg2;3hDk$c>(!^o~kyvJ:n*v&|ES');
define('LOGGED_IN_SALT',   'b,;oT[fbqEcso*1p&=4K~m1HJPJ48BxBWk)chwQeC]xu0y9F}10joL7Nf@:(%#UE');
define('NONCE_SALT',       '6&`5;e)T$2v48gU 1Pg*=EJ3%30.s[g}jCc2xeMVPdc[p|oeh^[rC*^c53$87<lW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
