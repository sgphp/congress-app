<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170401105219 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE answers');
        $this->addSql('ALTER TABLE questions ADD free TINYINT(1) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE answers (id INT AUTO_INCREMENT NOT NULL, variant_id INT DEFAULT NULL, template_id INT DEFAULT NULL, INDEX IDX_50D0C6065DA0FB8 (template_id), INDEX IDX_50D0C6063B69A9AF (variant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C6063B69A9AF FOREIGN KEY (variant_id) REFERENCES variants (id)');
        $this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C6065DA0FB8 FOREIGN KEY (template_id) REFERENCES templates (id)');
        $this->addSql('ALTER TABLE questions DROP free');
    }
}
