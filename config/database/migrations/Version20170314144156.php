<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170314144156
 * @package Migrations
 */
class Version20170314144156 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            'CREATE TABLE templates (
                id INT AUTO_INCREMENT NOT NULL,
                name VARCHAR(255) NOT NULL, 
                template LONGTEXT NOT NULL, 
                enabled TINYINT(1) NOT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE template_pipelines (
                id INT AUTO_INCREMENT NOT NULL, 
                template_id INT DEFAULT NULL, 
                division VARCHAR(255) DEFAULT NULL, 
                office VARCHAR(255) DEFAULT NULL, 
                official VARCHAR(255) DEFAULT NULL, 
                INDEX IDX_2D2F2F145DA0FB8 (template_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE template_pipelines ADD CONSTRAINT FK_2D2F2F145DA0FB8 FOREIGN KEY (template_id) REFERENCES templates (id)');
        $this->addSql('ALTER TABLE users CHANGE role role VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE template_pipelines DROP FOREIGN KEY FK_2D2F2F145DA0FB8');
        $this->addSql('DROP TABLE templates');
        $this->addSql('DROP TABLE template_pipelines');
        $this->addSql('ALTER TABLE users CHANGE role role VARCHAR(255) DEFAULT \'user\' COLLATE utf8_unicode_ci');
    }
}
