<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170321121219 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('CREATE TABLE variants (id INT AUTO_INCREMENT NOT NULL, question_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, compiled_value LONGTEXT NOT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_B39853E11E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, template_id INT DEFAULT NULL, message LONGTEXT NOT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_8ADC54D5F675F31B (author_id), INDEX IDX_8ADC54D55DA0FB8 (template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE answers (id INT AUTO_INCREMENT NOT NULL, template_id INT DEFAULT NULL, variant_id INT DEFAULT NULL, INDEX IDX_50D0C6065DA0FB8 (template_id), INDEX IDX_50D0C6063B69A9AF (variant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE variants ADD CONSTRAINT FK_B39853E11E27F6BF FOREIGN KEY (question_id) REFERENCES questions (id)');
        $this->addSql('ALTER TABLE questions ADD CONSTRAINT FK_8ADC54D5F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE questions ADD CONSTRAINT FK_8ADC54D55DA0FB8 FOREIGN KEY (template_id) REFERENCES templates (id)');
        $this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C6065DA0FB8 FOREIGN KEY (template_id) REFERENCES templates (id)');
        $this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C6063B69A9AF FOREIGN KEY (variant_id) REFERENCES variants (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE answers DROP FOREIGN KEY FK_50D0C6063B69A9AF');
        $this->addSql('ALTER TABLE variants DROP FOREIGN KEY FK_B39853E11E27F6BF');
        $this->addSql('DROP TABLE variants');
        $this->addSql('DROP TABLE questions');
        $this->addSql('DROP TABLE answers');
    }
}
