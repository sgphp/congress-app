<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170323103019
 * @package Migrations
 */
class Version20170323103019 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE settings ADD signature_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE settings ADD CONSTRAINT FK_E545A0C5ED61183A FOREIGN KEY (signature_id) REFERENCES signature (id)');
        $this->addSql('CREATE INDEX IDX_E545A0C5ED61183A ON settings (signature_id)');
        $this->addSql('ALTER TABLE signature ADD picture VARCHAR(255) DEFAULT NULL, ADD type VARCHAR(255) NOT NULL, ADD value VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE settings DROP FOREIGN KEY FK_E545A0C5ED61183A');
        $this->addSql('DROP INDEX IDX_E545A0C5ED61183A ON settings');
        $this->addSql('ALTER TABLE settings DROP signature_id');
        $this->addSql('ALTER TABLE signature DROP picture, DROP type, DROP value');
    }
}
