<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170406100441 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE created created DATETIME NOT NULL');
        $this->addSql('ALTER TABLE questions ADD multiple TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE templates CHANGE created created DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE questions DROP multiple');
        $this->addSql('ALTER TABLE templates CHANGE created created DATETIME DEFAULT \'1970-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE users CHANGE created created DATETIME DEFAULT \'1970-01-01 00:00:00\'');
    }
}
