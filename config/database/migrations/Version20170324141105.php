<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170324141105 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE officials (id INT AUTO_INCREMENT NOT NULL, office_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, address LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', party VARCHAR(255) NOT NULL, phones LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', emails LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', urls LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', photo_url VARCHAR(255) DEFAULT NULL, channels LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_6527C484FFA0C224 (office_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offices (id INT AUTO_INCREMENT NOT NULL, division_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_F574FF4C41859289 (division_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE divisions (id INT AUTO_INCREMENT NOT NULL, ocd_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE officials ADD CONSTRAINT FK_6527C484FFA0C224 FOREIGN KEY (office_id) REFERENCES offices (id)');
        $this->addSql('ALTER TABLE offices ADD CONSTRAINT FK_F574FF4C41859289 FOREIGN KEY (division_id) REFERENCES divisions (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE officials DROP FOREIGN KEY FK_6527C484FFA0C224');
        $this->addSql('ALTER TABLE offices DROP FOREIGN KEY FK_F574FF4C41859289');
        $this->addSql('DROP TABLE officials');
        $this->addSql('DROP TABLE offices');
        $this->addSql('DROP TABLE divisions');
    }
}
