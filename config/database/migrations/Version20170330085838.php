<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170330085838
 * @package Migrations
 */
class Version20170330085838 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letters ADD official_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE letters ADD CONSTRAINT FK_FB5524FB4D88E615 FOREIGN KEY (official_id) REFERENCES officials (id)');
        $this->addSql('CREATE INDEX IDX_FB5524FB4D88E615 ON letters (official_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letters DROP FOREIGN KEY FK_FB5524FB4D88E615');
        $this->addSql('DROP INDEX IDX_FB5524FB4D88E615 ON letters');
        $this->addSql('ALTER TABLE letters DROP official_id');
    }
}
