<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170328091013 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE template_pipelines ADD division_id INT DEFAULT NULL, ADD office_id INT DEFAULT NULL, ADD official_id INT DEFAULT NULL, DROP division, DROP office, DROP official');
        $this->addSql('ALTER TABLE template_pipelines ADD CONSTRAINT FK_2D2F2F1441859289 FOREIGN KEY (division_id) REFERENCES divisions (id)');
        $this->addSql('ALTER TABLE template_pipelines ADD CONSTRAINT FK_2D2F2F14FFA0C224 FOREIGN KEY (office_id) REFERENCES offices (id)');
        $this->addSql('ALTER TABLE template_pipelines ADD CONSTRAINT FK_2D2F2F144D88E615 FOREIGN KEY (official_id) REFERENCES officials (id)');
        $this->addSql('CREATE INDEX IDX_2D2F2F1441859289 ON template_pipelines (division_id)');
        $this->addSql('CREATE INDEX IDX_2D2F2F14FFA0C224 ON template_pipelines (office_id)');
        $this->addSql('CREATE INDEX IDX_2D2F2F144D88E615 ON template_pipelines (official_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE template_pipelines DROP FOREIGN KEY FK_2D2F2F1441859289');
        $this->addSql('ALTER TABLE template_pipelines DROP FOREIGN KEY FK_2D2F2F14FFA0C224');
        $this->addSql('ALTER TABLE template_pipelines DROP FOREIGN KEY FK_2D2F2F144D88E615');
        $this->addSql('DROP INDEX IDX_2D2F2F1441859289 ON template_pipelines');
        $this->addSql('DROP INDEX IDX_2D2F2F14FFA0C224 ON template_pipelines');
        $this->addSql('DROP INDEX IDX_2D2F2F144D88E615 ON template_pipelines');
        $this->addSql('ALTER TABLE template_pipelines ADD division VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD office VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD official VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP division_id, DROP office_id, DROP official_id');
    }
}
