<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170321110049
 * @package Migrations
 */
class Version20170321110049 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            'CREATE TABLE letters (
                id INT AUTO_INCREMENT NOT NULL, 
                status VARCHAR(255) DEFAULT \'open\' NOT NULL, 
                content LONGTEXT NOT NULL, 
                created_at DATETIME NOT NULL, 
                responsible_email VARCHAR(255) NOT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql('DROP TABLE steps');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('CREATE TABLE steps (id INT AUTO_INCREMENT NOT NULL, identifier VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, data LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\', status VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE letters');
    }
}
