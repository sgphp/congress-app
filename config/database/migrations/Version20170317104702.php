<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170317104702 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            'CREATE TABLE template_tags (
                id INT AUTO_INCREMENT NOT NULL, 
                template_id INT DEFAULT NULL, 
                tag_id INT DEFAULT NULL, 
                INDEX IDX_BA72BE4B5DA0FB8 (template_id), 
                INDEX IDX_BA72BE4BBAD26311 (tag_id), 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );

        $this->addSql('ALTER TABLE template_tags ADD CONSTRAINT FK_BA72BE4B5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates (id)');
        $this->addSql('ALTER TABLE template_tags ADD CONSTRAINT FK_BA72BE4BBAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE template_tags');
    }
}
