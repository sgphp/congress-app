<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170327114841 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transactions (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(255) DEFAULT \'wait\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE letters ADD transaction_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE letters ADD CONSTRAINT FK_FB5524FB2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transactions (id)');
        $this->addSql('CREATE INDEX IDX_FB5524FB2FC0CB0F ON letters (transaction_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letters DROP FOREIGN KEY FK_FB5524FB2FC0CB0F');
        $this->addSql('DROP TABLE transactions');
        $this->addSql('DROP INDEX IDX_FB5524FB2FC0CB0F ON letters');
        $this->addSql('ALTER TABLE letters DROP transaction_id');
    }
}
