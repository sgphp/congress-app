<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170321101705
 * @package Migrations
 */
class Version20170321101705 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE settings (id INT AUTO_INCREMENT NOT NULL, address_line VARCHAR(1023) NOT NULL, city VARCHAR(1023) NOT NULL, zip VARCHAR(255) NOT NULL, state VARCHAR(1023) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE signature (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users ADD settings_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E959949888 FOREIGN KEY (settings_id) REFERENCES settings (id)');
        $this->addSql('CREATE INDEX IDX_1483A5E959949888 ON users (settings_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E959949888');
        $this->addSql('DROP TABLE settings');
        $this->addSql('DROP TABLE signature');
        $this->addSql('DROP INDEX IDX_1483A5E959949888 ON users');
        $this->addSql('ALTER TABLE users DROP settings_id');
    }
}
