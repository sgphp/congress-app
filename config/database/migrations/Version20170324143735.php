<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170324143735 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD signature_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9ED61183A FOREIGN KEY (signature_id) REFERENCES signature (id)');
        $this->addSql('CREATE INDEX IDX_1483A5E9ED61183A ON users (signature_id)');
        $this->addSql('ALTER TABLE settings DROP FOREIGN KEY FK_E545A0C5ED61183A');
        $this->addSql('DROP INDEX IDX_E545A0C5ED61183A ON settings');
        $this->addSql('ALTER TABLE settings DROP signature_id');
        $this->addSql('ALTER TABLE signature ADD font VARCHAR(255) NOT NULL, CHANGE picture picture VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE settings ADD signature_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE settings ADD CONSTRAINT FK_E545A0C5ED61183A FOREIGN KEY (signature_id) REFERENCES signature (id)');
        $this->addSql('CREATE INDEX IDX_E545A0C5ED61183A ON settings (signature_id)');
        $this->addSql('ALTER TABLE signature DROP font, CHANGE picture picture VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9ED61183A');
        $this->addSql('DROP INDEX IDX_1483A5E9ED61183A ON users');
        $this->addSql('ALTER TABLE users DROP signature_id');
    }
}
