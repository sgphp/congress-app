<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20170329143301
 * @package Migrations
 */
class Version20170329143301 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letters ADD template_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE letters ADD CONSTRAINT FK_FB5524FB5DA0FB8 FOREIGN KEY (template_id) REFERENCES templates (id)');
        $this->addSql('CREATE INDEX IDX_FB5524FB5DA0FB8 ON letters (template_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE letters DROP FOREIGN KEY FK_FB5524FB5DA0FB8');
        $this->addSql('DROP INDEX IDX_FB5524FB5DA0FB8 ON letters');
        $this->addSql('ALTER TABLE letters DROP template_id');
    }
}
